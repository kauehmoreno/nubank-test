# Desafio Nubank

[![Nubank](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQbftDqmZ22v73psv_I5rcgWeJQJOB6UmfX0ZYJcG0ZfuZd1K7H)](https://nubank.com.br/)

# O que foi feito ? 
  - Api que lida com transação 
  - Cadastro de um customer 
  - consequentemente criação de uma account
  - possibilidade de receber transação externa(uma abstração conceitual para transação de outros bancos in/out)
  - transação internal entre contas 
  - checagem de saldo 
  - chegam de account
  - listagem de transações

### Tecnologia usada

* [Golang] - Usado para o backend e workers
* [Redis] - Usado para abstrair o conceito de filas e cache( separado em cluster e sentinel(para Queue) )
* [mysql] - Usado para persistir os dados
* [Docker] - Usado para facilitar a configuração do ambiente local

### Comandos

Para executar o projeto é preciso executar os seguintes comandos:

```sh
$ make build-docker
$ make run 
$ ou se preferir make run-watch
```

Para excecução dos testes: 
```sh
$ make unit-test
$ make integration-test
$ make performance-test (avalia alguns benchmarks sobre alguns packages e não na aplicação apenas para ver alocação de memória e cpu pprof)
```

Em caso de problemas para buildar o docker, verificar:
```sh
os paths de WORKDIR que ta definido como WORKDIR $GOPATH/src/gitlab.com/kauehmoreno/nubank-test

e verificar a copia copia do arquivo de settings apondando tbm para o path 

COPY --chown=nubank-test --from=builder /go/src/gitlab.com/kauehmoreno/nubank-test/deploy/settings_local.json /api/deploy/settings_local.json

Alterar essas linhas para o path onde o zip for feito para garantir que o docker consinga enxergar os arquivos para buildar corretamente.
```

### Explicação do desenvolvimento

Basicamente toda a operação de transação é enfileirada para acontecer em background de maneira assíncrona. O redis sentinel foi configurado no docker-compose para atender apenas esse modelo de Queue. 

Foi criado 2 workers que fazer as criações de account baseada na ação de criar um customer e as validações para consolidar uma transação.

Ao criar uma transação, algumas válidações são feitas(não entrando muito afundo em todas as possibilidades), todavia é verificado se o usuário está tentando enviar recurso para ele mesmo, se ele tem recurso para essa transação e se é uma transação externa. Após essas validações, a transação é criada com o status P(pendente) e enfileirada.

O worker pega executa na fila a cada 30 segundos(modelo configurado no setup da aplicação - pode-se passar outros tempos para o worker) e refaz algumas validações para de fato efetivar o outcome do enviador e income para o recebedor para depois alterar o status da transação.

O worker poderia ter sido criado num outro enviromment mas optei por apenas start no main como uma goroutine (um para cada worker ) por questões de simplicidade para execução do desafio. 

Caso hajá algum erro nesse processo do worker de retirar o dinheiro de uma conta para inserir em outra, ele entra em backoff e tenta refazer a operação, caso não consiga, se ele já retirou o valor do conta do customer ele recoloca e altera o status da transação para C(cancelado) ou N(negado) .

### TODO:
 - Deixei apenas montado as opções de schedule de uma transação 
 - Uma rota para refund 
 - Mais testes de integração 
 

### ROTAS DA API 

Criação de um customer:
```python
POST http://localhost/customer/register 
Body:
{
    "name":"Marcus Migule",
	"email":"marcus_miguel21@teste.com",
	"cpf":"052.050.111-11"
}

Response:  status 200
{
    "id": "43377d84-5392-4b90-a58a-4cb5e77f3bd3",
    "account_id": "4070485851514133010",
    "name": "Marcus Migule",
    "email": "marcus_miguel21@teste.com",
    "cpf": "052.050.111-11"
}

status != 200
{
    "message":"error blbalba"
	"status": "500"
}
```

Detalhes de um customer:
```python
GET http://localhost/customer/detail/:customerID

response 200
{
    "id": "43377d84-5392-4b90-a58a-4cb5e77f3bd3",
    "account_id": "4070485851514133010",
    "name": "Marcus Migule",
    "email": "marcus_miguel21@teste.com",
    "cpf": "052.050.111-11"
}

status != 200
{
    "message":"error blbalba"
	"status": "500"
}
```

Criar uma transação ENTRE customers:
```python
POST http://localhost/transaction
Body:
{
	"sender_acc_id": "14554460871432733661",
	"receiver_acc_id":"7859984839297931463",
	"amount":500.12
}

response 200
{
   "message":"Transaction was successfull processed",
   "status":200
}

status 406 
{
    "message": "Not enough balance to finish transaction",
    "status": 406
}

status 409 - quando usuário tenta enviar recurso para si mesmo
{
    "message": "Conflit accountID",
    "status": 406
}

status 500 
{
    "message": "Something bad happens please try again later",
    "status": 500
}

```



Criar uma transação externa IN(de outro banco para um customer):
```python
POST http://localhost/transaction/in
Body:
{
	"from": {
		"bank_name":"itau",
		"bank_number":265917,
		"bank_agency":2977,
		"user_name":"Carlos Maia",
		"user_identifier":"012.254.111-11"
	},
	"to":{
		"bank_name":"nubank",
		"bank_number":3163127046,
		"bank_agency":1010,
		"user_name":"Marcus Migule",
		"user_identifier":"052.050.111-11"
	},
	"amount":1239.21
}

response 200
{
   "message":"Transaction was successfull processed",
   "status":200
}

status 406 
{
    "message": "Not enough balance to finish transaction",
    "status": 406
}


status 404 - quando usuário que receberá o dinheiro não é encontrado como customer
{
    "message": "Customer not found",
    "status": 404
}

status 500 
{
    "message": "Something bad happens please try again later",
    "status": 500
}

```

Criar uma transação externa OUT(customer enviando para outro banco):
```python
POST http://localhost/transaction/out
Body:
{
	"from": {
	    "bank_name":"nubank",
		"bank_number":3163127046,
		"bank_agency":1010,
		"user_name":"Marcus Migule",
		"user_identifier":"052.050.111-11"
	},
	"to":{
	    "bank_name":"itau",
		"bank_number":265917,
		"bank_agency":2977,
		"user_name":"Carlos Maia",
		"user_identifier":"012.254.111-11"
	},
	"amount":1239.21
}

response 200
{
   "message":"Transaction was successfull processed",
   "status":200
}

status 406 
{
    "message": "Not enough balance to finish transaction",
    "status": 406
}

status 404 - quando usuário que envia o dinheiro não é encontrado como customer
{
    "message": "User does not have account",
    "status": 404
}

status 500 
{
    "message": "Something bad happens please try again later",
    "status": 500
}

```

Listar as transações de um customer:
```python
GET http://localhost/transactions/:customerID

response 200
[{
    `json:"id"`
	`json:"created_at"`
	`json:"scheduled" `
	`json:"scheduled_at"`
	`json:"sender_acc_id" `
	`json:"receiver_acc_id"`
	`json:"status" `
	`json:"amount" "`
	`json:"refund" db:"refund"`
	`json:"external_transaction"`
	`json:"bank_name" `
	`json:"bank_number"`
	`json:"bank_agency"`
	`json:"user_name" `
	`json:"user_identifier"`
}]

status 404 - 
{
    "message": "Customer not found",
    "status": 404
}

status 500 
{
    "message": "Something bad happens please try again later",
    "status": 500
}

```


Balanço de um customer :
```python
GET http://localhost/account/:customerID

response 200
{
	"balance":4322.21,
	"agency":1010,
	"number":2645843,
	"customer_id":"fad07929-9bb8-472c-a266-e0b0954b360b"
}

status 404 - 
{
    "message": "Customer not found",
    "status": 404
}

status 500 
{
    "message": "Something bad happens please try again later",
    "status": 500
}
```

Healtcheck :
```python
GET http://localhost/healthcheck
Response:
status 200
WORKING! 

status 500
NOT WORKING! 

GET http://localhost/healthcheck/cache
Response:
status 200
WORKING! 

status 500
NOT WORKING! 

GET http://localhost/healthcheck/queue
Response:
status 200
WORKING! 

status 500
NOT WORKING! 

```

#### Testes
Há um json importado do postman para ser usado para facilitar os testes .

### Disclaimer 

O redis cluster não ficou com a configuração que eu queria, deu erro para buildar no docker compose devido aos hosts dinamicos - os slaves não estavam no sync com o master - toda as operações estavam dando erro menos o ping
Com isso modifiquei para o redis normal.


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
