FROM golang:alpine AS builder

RUN adduser -h $GOPATH -D -g '' nubank-test
USER nubank-test

WORKDIR $GOPATH/src/gitlab.com/kauehmoreno/nubank-test
ADD --chown=nubank-test . .

RUN go build -o /tmp/nubank-test main.go

FROM alpine:latest

RUN apk add -U tzdata \
    && cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \
    && echo "America/Sao_Paulo" > /etc/timezone \
    && apk del tzdata

RUN adduser -h /api -D -g '' nubank-test
USER nubank-test

WORKDIR /api

COPY --chown=nubank-test --from=builder /tmp/nubank-test /api/nubank-test
COPY --chown=nubank-test --from=builder /go/src/gitlab.com/kauehmoreno/nubank-test/deploy/settings_local.json /api/deploy/settings_local.json

EXPOSE 9000

ENTRYPOINT /api/nubank-test
