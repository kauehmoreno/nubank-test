bootstrap-test:
	@ginkgo bootstrap
# Generate interface mock
# mockgen -source=example.go -destination=api/mock/example.go

clear = \033[00m
green = \033[0;92m
red = \033[0;91m
redInverted = \033[1;41m
done = echo "${green}Done${clear}"; echo
deploy_list_cleanup = settings.json nubank-test
deploy_list = ${deploy_list_cleanup}
deploy_finalizado = echo "${green}Deploy finalizado!${clear}"

setup-dev:
	go get github.com/onsi/ginkgo/ginkgo
	go get github.com/onsi/gomega/...

setup: setup-dev build-docker

run:
	@docker-compose up -d

run-watch:
	@docker-compose up

down:
	@docker-compose down

full-down:
	@docker-compose down -v --remove-orphans

build-docker:
	@docker-compose build

unit-test:
	@ginkgo -cover  -tags unit -r -skipMeasurements ${TEST_PACKAGES}

performance-test:
	@ginkgo -cover -r -v -tags benchmark -race ${TEST_PACKAGES} 

integration-ginko-test:
	@ginkgo -cover -r -skipMeasurements -tags integration ${TEST_PACKAGES}


integration-test:
	@go test -v -count=1 ./api/handler/...
# @go test -cover -r -v -tags integration ./api/handler/handler_integration_test.go
# go test -cover -r -v -tags benchmark -race -bench=. -benchmem  ${TEST_PACKAGES} 


deploy-prod:
	@ENV=prod $(MAKE) deploy-ambiente

build:
	@echo "${blue}Iniciando compilação dos binários com ${yellow}GOOS=linux e GOARCH=amd64${clear}"
	@echo \

	@echo "${blue}Compilando nubank-test...${clear}"
	@go build -o nubank-test
	@${done}

deploy-cleanup:
	@echo "${blue}Limpando arquivos gerados no deploy${clear}"
	rm -f ${deploy_list_cleanup}
	@${done}

copy-files:
	@echo "${blue}Copiando arquivos necessários${clear}"
	cp -f deploy/settings_${ENV}.json settings.json
	@${done}
