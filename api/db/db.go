package db

import (
	"crypto/rand"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"sync"

	"github.com/cenkalti/backoff"
	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kauehmoreno/nubank-test/api/settings"

	log "github.com/sirupsen/logrus"
)

var (
	once sync.Once
	pool = sync.Pool{
		New: func() interface{} {
			return instance()
		},
	}
	database *mySQL
)

const (
	// NoRowsAffected identifies when operation of exec does not affect any row
	NoRowsAffected int64 = 0
	// DuplicatedEntity identifies error number of mysqldriver
	DuplicatedEntity uint16 = 1062
)

// ErrNoAffectedRow explicit tells to client whenever a exec affect or not a row into DB
var (
	ErrNoAffectedRow = errors.New("No rows got affect by this operation")
	ErrDuplicated    = errors.New("Duplicated entity error - primary key already exists on db")
)

type DB interface {
	Exec(query string, params ...interface{}) (sql.Result, error)
	Query(dest interface{}, query string, params ...interface{}) error
	Get(dest interface{}, query string, params ...interface{}) error
}

// mySQL has both read and write connection point of db
// it separeated logicly to be able to scale both read and write instance on demand
type mySQL struct {
	readCon  *sqlx.DB
	writeCon *sqlx.DB
}

func (m mySQL) Exec(query string, params ...interface{}) (sql.Result, error) {
	result, err := m.writeCon.Exec(query, params...)
	if err != nil {
		if driverErr, ok := err.(*mysql.MySQLError); ok {
			if driverErr.Number == DuplicatedEntity {
				return result, ErrDuplicated
			}
		}
		return result, err
	}
	return result, nil
}

func (m mySQL) Query(dest interface{}, query string, params ...interface{}) error {
	return m.readCon.Select(dest, query, params...)
}

func (m mySQL) Get(dest interface{}, query string, params ...interface{}) error {
	return m.readCon.Get(dest, query, params...)
}

// Ping used to healthcheck
func (m mySQL) Ping() error {
	return m.readCon.Ping()
}

func startReadDB() *sqlx.DB {
	return start(settings.Get().DB.Read)
}

func startWriteDB() *sqlx.DB {
	return start(settings.Get().DB.Write)
}

func start(settings settings.DatabaseSettings) *sqlx.DB {
	var conn *sqlx.DB
	log.Info("Connecting on DB...")
	DSN := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?timeout=%s", settings.User, settings.Password, settings.Host, settings.Port, settings.Name, settings.Timeout)

	tryConn := func() error {
		connection, err := sqlx.Connect("mysql", DSN)
		if err != nil {
			log.Errorf("Erro on connect %s - retrying", err)
			return err
		}
		conn = connection
		return nil
	}

	log.Infof("DB data source network: %s", DSN)
	if err := backoff.Retry(tryConn, backoff.NewExponentialBackOff()); err != nil {
		log.Fatalf("Fail on stabilish database connection :%s - server: %s - host: %s", err, settings.Name, settings.Host)
	}

	conn.SetMaxIdleConns(settings.MaxIdle)
	conn.SetMaxOpenConns(settings.MaxConn)
	conn.SetConnMaxLifetime(settings.ConnMaxLifetime)

	healthcheck := func() error {
		if err := conn.Ping(); err != nil {
			log.Warning("Ping falhou! Tentando novamente")
			return err
		}
		return nil
	}

	if err := backoff.Retry(healthcheck, backoff.NewExponentialBackOff()); err != nil {
		log.Fatalf("Fail on stabilish database connection :%s - server: %s - host: %s", err, settings.Name, settings.Host)
	}
	return conn
}

func instance() *mySQL {
	once.Do(func() {
		database = &mySQL{
			readCon:  startReadDB(),
			writeCon: startWriteDB(),
		}
	})
	return database
}

// GetInstance return a single instance for client
// use sync pool to avoid pressure on GC on everytime a instance be called
// the allocation per operation will be few which will gain performance
func GetInstance() DB {
	defer pool.Put(instance())
	db := pool.Get().(*mySQL)
	return db
}

// GenerateUUID used to perfom id on every DB entity
func GenerateUUID() string {
	uuid := make([]byte, 16)
	io.ReadFull(rand.Reader, uuid)
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
}
