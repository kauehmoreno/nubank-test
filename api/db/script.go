package db

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/kauehmoreno/nubank-test/api/settings"
)

const ambiente = "local"

// CreateTables used only on local development
func CreateTables() error {
	if settings.Get().Ambiente == ambiente {
		database := GetInstance()
		if _, err := database.Exec(createDbQueryCustomer()); err != nil {
			log.Errorf("Error on create customer table with indices err: %s", err)
			return err
		}

		if _, err := database.Exec(createDBQueryAccount()); err != nil {
			log.Errorf("Error on create account table with indices err: %s", err)
			return err
		}

		if _, err := database.Exec(createDBQueryTransaction()); err != nil {
			log.Errorf("Error on create account table with indices err: %s", err)
			return err
		}
		return nil
	}
	log.Info("It's not a development env - it will no execute script to create table")
	return nil
}

func createDbQueryCustomer() string {
	return `
	CREATE TABLE IF NOT EXISTS customer (
		id varchar(36) not null,
		name varchar(255),
		email varchar(255),
		cpf varchar(15) not null,
		account_id varchar(36) not null,
		PRIMARY KEY (id),
		INDEX ix_account_id (account_id),
		INDEX ix_cpf_id (cpf),
		INDEX ix_email (email),
		UNIQUE INDEX ix_email_cpf (email, cpf)
	);`
}

func createDBQueryAccount() string {
	return `
	CREATE TABLE IF NOT EXISTS account (
		id varchar(36) not null,
		agency int(4) unsigned not null,
		number bigint(8) unsigned not null,
		balance DOUBLE(11,2) not null,
		PRIMARY KEY (id),
		INDEX ix_number (number),
		UNIQUE INDEX ix_id_number (id, number)
	);`
}

func createDBQueryTransaction() string {
	return `
	CREATE TABLE IF NOT EXISTS transaction (
		id varchar(36) not null,
		created_at DATETIME not null,
		scheduled_at DATETIME not null,
		scheduled TINYINT(1) DEFAULT 0,
		sender_acc_id varchar(36),
		receiver_acc_id varchar(36),
		status CHAR(1) not null,
		amount DOUBLE(11, 2) not null,
		refund TINYINT(1) DEFAULT 0,
		external_transaction TINYINT(1) DEFAULT 0,
		bank_name varchar(255),
		bank_number bigint(8) unsigned,
		bank_agency int(4) unsigned,
		user_name varchar(255),
		user_identifier varchar(14),
		PRIMARY KEY (id),
		INDEX ix_scheduled_at (scheduled_at),
		INDEX ix_receiver_acc_id (receiver_acc_id),
		INDEX ix_sender_acc_id (sender_acc_id),
		UNIQUE INDEX ix_id (id)
	  );`
}
