package queue

import (
	"sync"

	"github.com/go-redis/redis"
	"gitlab.com/kauehmoreno/nubank-test/api/cache"
	"gitlab.com/kauehmoreno/nubank-test/api/settings"

	"github.com/vmihailenco/msgpack"
)

// Queue is a interface which defines a queue implementation
type Queue interface {
	Push(queueName string, msg interface{}) error
	Purge(queueName string) error
	GetMessage(queueName string, expected interface{}) error
	Len(queueName string) int64
}

const (
	// CreateAccount is a queue name which is resposable to delegate account creation based on user registration
	CreateAccount = "CREATE_ACCOUNT"
	// TrasanctionCreated queue name to specify that a transaction was created
	TrasanctionCreated = "TRANSACTION_CREATED"
)

var (
	once     sync.Once
	queueCli *QueueCli
	pool     = sync.Pool{
		New: func() interface{} {
			return start()
		},
	}
)

type QueueCli struct {
	conn *redis.Client
}

func options() redis.Options {
	return redis.Options{
		Addr:        settings.Get().Queue.Address[0],
		Password:    settings.Get().Queue.Passwd,
		DB:          0,
		PoolSize:    settings.Get().Queue.PoolSize,
		PoolTimeout: settings.Get().Queue.PoolTimeout,
		IdleTimeout: settings.Get().Queue.IdleTimeout,
		MaxRetries:  2,
	}
}

// GetInstance is used by external client package api
func GetInstance() *QueueCli {
	defer pool.Put(start())
	q := pool.Get().(*QueueCli)
	return q
}

// Start used to start queue instance on aplication
// it must be used as a initializer on aplication
// client should use only Push/Purge/GetMessage directly
func start() *QueueCli {
	once.Do(func() {
		opt := options()
		queueCli = &QueueCli{
			conn: redis.NewClient(&opt),
		}
	})
	return queueCli
}

// Push based on a queueName will put a msg into it
func (queue QueueCli) Push(queueName string, msg interface{}) error {
	queueName = cache.HashKey(queueName)
	bmsg, err := msgpack.Marshal(msg)
	if err != nil {
		return err
	}
	return queue.conn.LPush(queueName, bmsg).Err()
}

// Purge will delete all contents inside a queue
func (queue QueueCli) Purge(queueName string) error {
	queueName = cache.HashKey(queueName)
	return queue.conn.Del(queueName).Err()
}

// Ping use to healtcheck
func (queue QueueCli) Ping() error {
	return queue.conn.Ping().Err()
}

// GetMessage will retrieve first input message
// it's a FIFO queue
func (queue QueueCli) GetMessage(queueName string, expected interface{}) error {
	queueName = cache.HashKey(queueName)

	result, err := queue.conn.RPop(queueName).Bytes()
	if err != nil {
		return err
	}
	return msgpack.Unmarshal(result, &expected)
}

// Len it returns the number of items inside a queue
// this operation has a complexity of O(1)
// guaranted by redis https://redis.io/commands/llen
func (queue QueueCli) Len(queueName string) int64 {
	queueName = cache.HashKey(queueName)
	return queue.conn.LLen(queueName).Val()
}
