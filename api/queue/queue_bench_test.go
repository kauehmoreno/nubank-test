// +test benchmark

package queue_test

import (
	"fmt"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/kauehmoreno/nubank-test/api/queue"
)

var _ = Describe("Benchamrk test queue instance", func() {

	Describe("Queue GetInstance", func() {
		Measure("It should execute many times and allocate less memory/operation as well as time should be low", func(b Benchmarker) {
			runtime := b.Time("runtime", func() {
				q := queue.GetInstance()
				Expect(q).ShouldNot(BeNil())
			})

			Ω(runtime.Seconds()).Should(BeNumerically("<", 0.1), "Shouldn't take too long")
			b.RecordValue("disk usage", 100)
		}, 2048)
	})
})

func BenchmarkTestOnGettingQueueInstance(b *testing.B) {
	for i := 1; i <= 2048; i *= 2 {
		b.Run(fmt.Sprintf("%d\n", i), func(b *testing.B) {
			for n := 0; n <= b.N; n++ {
				if err := queue.GetInstance(); err != nil {
					b.FailNow()
				}
			}
		})
	}
}

func BenchmarkTestOnGettingQueueInstanceInParallel(b *testing.B) {
	for i := 1; i <= 2048; i *= 2 {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				if err := queue.GetInstance(); err != nil {
					b.FailNow()
				}
			}
		})
	}
}
