package api

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
)

// ServerMock is used to allow easy testing module on request
func ServerMock(statusCode int, msg string) *httptest.Server {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json;charset=UTF-8")
		w.Header().Set("Vary", "Accept-Encoding")
		w.WriteHeader(statusCode)
		data := struct {
			Message string `json:"message"`
			Status  int    `json:"status"`
		}{msg, statusCode}
		json.NewEncoder(w).Encode(&data)
	}))
	return ts
}
