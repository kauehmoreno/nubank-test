package cache

import "time"

const (
	// NoExpiration is infinity cache
	NoExpiration = time.Duration(0)

	// KeyUser is the partner of user identifie on cache
	KeyUser = "KEY_USER"
	// KeyByCPF is anoter key used to get user
	KeyByCPF = "KEY_BY_CPF"
	// KeyCustomerAccount is a key to identify customer account
	KeyCustomerAccount = "CUSTOMER_ACCOUNT"
	// KeyTransaction is a key to identify each transaction
	KeyTransaction = "TRANSACTION_ID"
	// KeyUserTransactions all user transactions
	KeyUserTransactions = "USER_TRANSACTION"
	// KeyQtdUserTransaction qtd of transactions per user
	KeyQtdUserTransaction = "QTD_USER_TRANSACTION"
)
