package cache

import "time"

// Cache interface is the minimum  cache representation on system
type Cache interface {
	Get(key string, value interface{}) error
	Set(key string, value interface{}, exp time.Duration) error
	Delete(key ...string) error
}

// HealthCache interface responsable to define contract of healthcheck Cache action
type HealthCache interface {
	Ping() error
}

// ListCache is an interface which defined contract of list cache operation
// it was created to turn it able composition
type ListCache interface {
	LPush(key string, values ...interface{}) error
	LRange(key string, from, to int64) ([]string, error)
	LRem(key string, value interface{}) error
	LLen(key string) (int64, error)
}
