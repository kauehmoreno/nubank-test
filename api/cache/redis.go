package cache

import (
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/cespare/xxhash"
	"github.com/go-redis/redis"
	"github.com/vmihailenco/msgpack"
	"gitlab.com/kauehmoreno/nubank-test/api/settings"
)

var (
	once        sync.Once
	clusterPool *RedisCluster
	pool        = sync.Pool{
		New: func() interface{} {
			return newPool()
		},
	}
)

// RedisCluster is a composition of ClusterClient and does not allow client
// call struct directly forcing usage of GetInstance to allow client to get
// benefit of singleton and sync.pool
// based on that cluster is a nonexported attr
type RedisCluster struct {
	cluster *redis.Client
}

// Get based on a key it look for result and marshal to value expected
// internally it uses hasKey for better key's shards between cluster and msgpack
// to short value size
func (c RedisCluster) Get(key string, value interface{}) error {
	k := HashKey(key)
	result, err := c.cluster.Get(k).Bytes()
	if err != nil {
		return err
	}
	return msgpack.Unmarshal(result, &value)
}

// Set value on cache using mspack to zip value and hashkey to better shard of keys
// through clusters
func (c RedisCluster) Set(key string, value interface{}, exp time.Duration) error {
	key = HashKey(key)
	result, err := msgpack.Marshal(value)
	if err != nil {
		return err
	}
	return c.cluster.Set(key, result, exp).Err()
}

// Ping usado para healthcheck
func (c RedisCluster) Ping() error {
	return c.cluster.Ping().Err()
}

// Delete keys on bulk and single operation
func (c RedisCluster) Delete(keys ...string) error {
	hashKeys := []string{}
	for _, k := range keys {
		k = HashKey(k)
		hashKeys = append(hashKeys, k)
	}
	return c.cluster.Del(hashKeys...).Err()
}

// Expire is used to expire keys executed on other commands besides set as lpush and so on
func (c RedisCluster) Expire(key string, exp time.Duration) error {
	k := HashKey(key)
	return c.cluster.Expire(k, exp).Err()
}

// LPush store into list an item packed by msgpack
func (c RedisCluster) LPush(key string, values ...interface{}) error {
	k := HashKey(key)
	return c.cluster.LPush(k, values...).Err()
}

// LRange get a number of elements based on from and to
func (c RedisCluster) LRange(key string, from, to int64) ([]string, error) {
	k := HashKey(key)
	return c.cluster.LRange(k, from, to).Result()
}

// LRem is used to remove a item from a list based on value matching
func (c RedisCluster) LRem(key string, value interface{}) error {
	k := HashKey(key)
	return c.cluster.LRem(k, 1, value).Err()
}

// LLen is used to get size of a list this operation has complexy O(1)
func (c RedisCluster) LLen(key string) (int64, error) {
	k := HashKey(key)
	return c.cluster.LLen(k).Result()
}

// GetInstance return a unique instance of redis cluster on pool as singleton
// low memory footprint overhead
func GetInstance() *RedisCluster {
	defer pool.Put(newPool())
	cluster := pool.Get().(*RedisCluster)
	return cluster
}

func newPool() *RedisCluster {
	once.Do(func() {
		options := conf()
		clusterPool = &RedisCluster{
			cluster: redis.NewClient(&options),
		}
	})
	return clusterPool
}

func conf() redis.Options {
	return redis.Options{
		Addr:        settings.Get().Redis.Address[0],
		Password:    settings.Get().Redis.Passwd,
		DB:          0,
		PoolSize:    settings.Get().Redis.PoolSize,
		PoolTimeout: settings.Get().Redis.PoolTimeout,
		IdleTimeout: settings.Get().Redis.IdleTimeout,
		MaxRetries:  2,
	}
}

// FormatKey combine all params to a pattern as %s:%s:%s
// and its use to cache formatKey parttern
func FormatKey(name string, params ...string) string {
	var s strings.Builder
	s.WriteString(name)
	for _, param := range params {
		s.WriteString(":")
		s.WriteString(param)
	}
	return s.String()
}

// HashKey hash key to make easy to cache system like redis
// distribute keys among all clusters
// as well as comparision matches become faster internally
func HashKey(k string) string {
	return strconv.FormatUint(xxhash.Sum64String(k), 10)
}
