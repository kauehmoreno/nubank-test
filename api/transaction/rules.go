package transaction

import (
	"errors"

	"gitlab.com/kauehmoreno/nubank-test/api/customer"

	"gitlab.com/kauehmoreno/nubank-test/api/account"
)

var (
	// ErrNoEnoughBalance error to determine if use is allow to transfer money or not based on balance
	ErrNoEnoughBalance = errors.New("User does not have enough balance to execute transaction")
	// ErrConflictAccountID error when a accountID try to send money to same one
	ErrConflictAccountID = errors.New("Customer is trying to send money to it self")
)

// Rules set all rules to allow a transaction
type Rules func(t *Transaction) error

// EnoughBalance based on transaction looks for an account and check balance to allow transaction
// from sender
func EnoughBalance(acc account.Acc) Rules {
	return func(t *Transaction) error {
		account, err := acc.Account(t.SenderAccID)
		if err != nil {
			return err
		}
		if t.Amount > account.GetBalance() {
			return ErrNoEnoughBalance
		}
		return nil
	}
}

// CheckReceiverAndSender check if user exist and check if user is tr
func CheckReceiverAndSender() Rules {
	return func(t *Transaction) error {
		if t.ReceiverAccID == t.SenderAccID {
			return ErrConflictAccountID
		}
		return nil
	}
}

// WithInsideCustomerValidation check when a outcome transaction happens
// will try to find customer to allow or deny transaction
func WithInsideCustomerValidation(transferIn bool, bankInfo account.ExternalAccountInfo) Rules {
	return func(t *Transaction) error {
		manager := customer.NewManager()
		var c customer.Customer
		err := manager.ByCPF(&c, bankInfo.Identifier)
		if err != nil {
			return err
		}

		if transferIn {
			t.ReceiverAccID = c.AccountID
			return nil
		}
		t.SenderAccID = c.AccountID
		return nil
	}
}
