package transaction

import (
	"gitlab.com/kauehmoreno/nubank-test/api/account"
)

// OutTrasanction its the representation of a outside transaction
// when customer from other banks must transfer money to a inside client
type OutTrasanction struct {
	From   account.ExternalAccountInfo `json:"from"`
	To     account.ExternalAccountInfo `json:"to"`
	Amount float64                     `json:"amount"`
}

// InternalTransactionPayload is a representation of payload to transfer money between internal accounts
type InternalTransactionPayload struct {
	SenderAccID   string  `json:"sender_acc_id"`
	ReceiverAccID string  `json:"receiver_acc_id"`
	Amount        float64 `json:"amount"`
}
