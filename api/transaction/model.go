package transaction

import (
	"time"

	"gitlab.com/kauehmoreno/nubank-test/api/account"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

const (
	// Pending - transaction's current status
	Pending = "P"
	// Accept - transaction's current status
	Accept = "A"
	// Deny - transaction's current status
	Deny = "N"
	// Refund  - transaction's current status
	Refund = "R"
	// Canceled - transaction's current status
	Canceled = "C"
)

// Transaction is the struct responsable for obtain all systems transaction
type Transaction struct {
	ID                  string    `json:"id" db:"id"`
	CreatedAt           time.Time `json:"created_at" db:"created_at"`
	Scheduled           bool      `json:"scheduled" db:"scheduled"`
	ScheduledAt         time.Time `json:"scheduled_at" db:"scheduled_at"`
	SenderAccID         string    `json:"sender_acc_id" db:"sender_acc_id"`
	ReceiverAccID       string    `json:"receiver_acc_id" db:"receiver_acc_id"`
	Status              string    `json:"status" db:"status"`
	Amount              float64   `json:"amount" db:"amount"`
	Refund              bool      `json:"refund" db:"refund"`
	ExternalTransaction bool      `json:"external_transaction" db:"external_transaction"`
	account.ExternalAccountInfo
}

// IsExternalTransaction allow client to check with a transaction is external
func (t Transaction) IsExternalTransaction() bool {
	return t.ExternalTransaction
}

// Option allow client to determine a transaction confi
// for example - if there it's a postponed one or what amount of money is whithin
// transaction and if it's a external one or not
type Option func(t *Transaction)

// WithSchedule allow client to determine if this transaction will be postponed or not
func WithSchedule(addDays int) Option {
	return func(tr *Transaction) {
		now := time.Now()
		tr.Scheduled = true
		tr.ScheduledAt = now.AddDate(0, 0, addDays)
	}
}

// WithAmount allow client to determine the amount of this transaction
func WithAmount(amount float64) Option {
	return func(tr *Transaction) {
		tr.Amount = amount
	}
}

// WithExternalTransaction explicite allow to define with it's a in house transaction or
// among external banks
func WithExternalTransaction(externalInfo account.ExternalAccountInfo) Option {
	return func(tr *Transaction) {
		tr.ExternalTransaction = true
		tr.BankAgency = externalInfo.BankAgency
		tr.Bank = externalInfo.Bank
		tr.BankNumber = externalInfo.BankNumber
		tr.UserName = externalInfo.UserName
		tr.Identifier = externalInfo.Identifier
	}
}

// New allow client to create a transaction
func New(sender, receiver string, opts ...Option) Transaction {
	now := time.Now()
	tr := Transaction{
		ID:                  db.GenerateUUID(),
		CreatedAt:           now,
		Status:              Pending,
		ExternalTransaction: false,
		Scheduled:           false,
		ScheduledAt:         now,
		SenderAccID:         sender,
		ReceiverAccID:       receiver,
		Refund:              false,
		Amount:              0,
	}

	for _, opt := range opts {
		opt(&tr)
	}
	return tr
}
