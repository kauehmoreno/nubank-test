package transaction_test

import (
	"time"

	"gitlab.com/kauehmoreno/nubank-test/api/account"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
	"gitlab.com/kauehmoreno/nubank-test/api/transaction"
)

var _ = Describe("Model package transaction", func() {
	var (
		sender, receiver string
	)
	BeforeEach(func() {
		sender = db.GenerateUUID()
		receiver = db.GenerateUUID()
	})
	Context("New instance", func() {
		It("Should return default values on new is call", func() {
			tr := transaction.New(sender, receiver)

			Expect(tr.SenderAccID).Should(Equal(sender))
			Expect(tr.ReceiverAccID).Should(Equal(receiver))
			Expect(tr.ID).ShouldNot(Equal(""))
		})
		It("Should always on call new return pending status", func() {
			tr := transaction.New(sender, receiver)

			Expect(tr.Status).Should(Equal(transaction.Pending))
		})
		It("Should have bool fields on default false on new call", func() {

			tr := transaction.New(sender, receiver)

			Expect(tr.Scheduled).Should(BeFalse())
			Expect(tr.Refund).Should(BeFalse())
		})

		It("Should mark external Transaction when opt is passed", func() {
			externalInfo := account.ExternalAccountInfo{
				BankAgency: 3213,
				BankNumber: 43242342,
				UserName:   "teste@cmeail.com",
				Identifier: "055.055.44-444",
			}
			tr := transaction.New(sender, receiver, transaction.WithExternalTransaction(externalInfo))
			Expect(tr.IsExternalTransaction()).Should(BeTrue())
			Expect(tr.Bank).Should(Equal(externalInfo.Bank))
		})
		It("Should change amount on transaction when opt is passed", func() {
			tr := transaction.New(sender, receiver, transaction.WithAmount(33.21))
			Expect(tr.Amount).Should(Equal(33.21))
		})
		It("Should change scheduledAt on transaction when opt is passed", func() {
			tr := transaction.New(sender, receiver, transaction.WithSchedule(2))
			Expect(tr.Scheduled).Should(BeTrue())
			now := time.Now()
			Expect(tr.ScheduledAt.Day()).ShouldNot(Equal(now.Day()))
		})
	})
})
