package transaction

import (
	"errors"

	"github.com/go-redis/redis"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/kauehmoreno/nubank-test/api/cache"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

var _ = Describe("Package transaction - repository", func() {

	var (
		mockCache     *cache.MockCache
		mockListCache *cache.MockListCache
		mockDB        *db.MockDB
		mockCtrl      *gomock.Controller
		repo          repository
		id            = db.GenerateUUID()
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockCache = cache.NewMockCache(mockCtrl)
		mockDB = db.NewMockDB(mockCtrl)
		mockListCache = cache.NewMockListCache(mockCtrl)

		repo.cacheRepo = mockCache
		repo.database = mockDB
		repo.listRep = mockListCache

	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Describe("Repository implementation of transference methods interface", func() {
		Context("ByID", func() {
			It("Should always look for content on cache first if ok return it", func() {
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil).Times(1)
				_, err := repo.ByID(id)
				Expect(err).To(BeNil())
			})
			It("Should execute db if cache miss hit and then set on cache", func() {
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(redis.Nil).Times(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(1)
				repo.ByID(id)
			})
			It("Should return erro whenever db returns error and does not execut set on cache", func() {
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(redis.Nil).Times(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("error db")).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(0)
				_, err := repo.ByID(id)
				Expect(err.Error()).Should(Equal("error db"))
			})
		})
		Context("Create", func() {
			It("Should create transaction first on db and set caches (set/lpush/lpush)", func() {
				sender := db.GenerateUUID()
				receiver := db.GenerateUUID()
				tr := New(sender, receiver)

				sqlResult := db.NewMockSQLResult(nil, 0, 1)

				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(sqlResult, nil).AnyTimes()
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
				mockListCache.EXPECT().LPush(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
				err := repo.Create(&tr)
				Expect(err).To(BeNil())
			})
			It("Should create transaction first on db and set caches (set/lpush) with error only log and keep going", func() {
				sender := db.GenerateUUID()
				receiver := db.GenerateUUID()
				tr := New(sender, receiver)

				sqlResult := db.NewMockSQLResult(nil, 0, 1)

				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(sqlResult, nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("error cache")).Times(1)
				mockListCache.EXPECT().LPush(gomock.Any(), gomock.Any()).Return(errors.New("error cache")).Times(0)
				err := repo.Create(&tr)
				Expect(err).To(BeNil())
			})

			It("Should match params on query db", func() {
				sender := db.GenerateUUID()
				receiver := db.GenerateUUID()
				tr := New(sender, receiver)
				sqlResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Do(func(query string, params ...interface{}) {
					id := params[0].(string)
					Expect(id).Should(Equal(tr.ID))
				}).Return(sqlResult, errors.New("error")).Times(1)

				err := repo.Create(&tr)
				Expect(err).ToNot(BeNil())
			})

			It("Should return ErrNoAffectedRow from db in case of no rows affected", func() {
				sender := db.GenerateUUID()
				receiver := db.GenerateUUID()
				tr := New(sender, receiver)

				sqlResult := db.NewMockSQLResult(db.ErrNoAffectedRow, 0, 1)

				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Do(func(query string, params ...interface{}) {
					id := params[0].(string)
					Expect(id).Should(Equal(tr.ID))
				}).Return(sqlResult, nil).Times(1)

				err := repo.Create(&tr)
				Expect(err).ToNot(BeNil())
				Expect(err).Should(Equal(db.ErrNoAffectedRow))
			})
		})
		Context("ChangeStatus", func() {
			It("Should change status on db and get element on cache change and set", func() {
				sqlResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(sqlResult, nil).Times(1)
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(1)
				err := repo.ChangeStatus(id, Accept)
				Expect(err).To(BeNil())
			})

			It("Should return error if db fails", func() {
				sqlResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(sqlResult, errors.New("error db")).Times(1)
				err := repo.ChangeStatus(id, Accept)
				Expect(err).ToNot(BeNil())
				Expect(err.Error()).Should(Equal("error db"))
			})
			It("Should return ErrNoAffectedRow if no rows got affected ", func() {
				sqlResult := db.NewMockSQLResult(db.ErrNoAffectedRow, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(sqlResult, nil).Times(1)
				err := repo.ChangeStatus(id, Accept)
				Expect(err).ToNot(BeNil())
				Expect(err).Should(Equal(db.ErrNoAffectedRow))
			})
			It("Should try to delete key if get on cache fail", func() {
				sqlResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(sqlResult, nil).Times(1)
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(errors.New("error cache")).Times(1)
				mockCache.EXPECT().Delete(gomock.Any()).Times(1)
				err := repo.ChangeStatus(id, Accept)
				Expect(err).To(BeNil())
			})
		})
		Context("Transactions", func() {
			repo.offSetLimit = 30
			It("Should first see len of list of ids", func() {
				mockListCache.EXPECT().LLen(gomock.Any()).Return(int64(0), errors.New("erro caches")).Times(1)
				tr, err := repo.Transactions(id)
				Expect(err).ToNot(BeNil())
				Expect(tr).Should(HaveLen(0))
			})
			It("Should execute Lrange after Llen return num > 0", func() {
				mockListCache.EXPECT().LLen(gomock.Any()).Return(int64(1), nil).Times(1)
				fakeIDs := []string{"12", "11"}
				mockListCache.EXPECT().LRange(gomock.Any(), int64(0), repo.offSetLimit).Return(fakeIDs, errors.New("error")).Times(1)
				tr, err := repo.Transactions(id)
				Expect(err).ToNot(BeNil())
				Expect(tr).Should(HaveLen(0))
			})
			It("Should execute ByID whenever Lrange return list of ids", func() {
				mockListCache.EXPECT().LLen(gomock.Any()).Return(int64(1), nil).Times(1)
				fakeIDs := []string{"12", "11"}
				mockListCache.EXPECT().LRange(gomock.Any(), int64(0), repo.offSetLimit).Return(fakeIDs, nil).Times(1)

				// mock byID
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil).MaxTimes(2)
				_, err := repo.Transactions(id)
				Expect(err).To(BeNil())
			})
			It("Should return error if searchTransaction return error on byID operations after Lrange return id", func() {
				mockListCache.EXPECT().LLen(gomock.Any()).Return(int64(1), nil).Times(1)
				fakeIDs := []string{"12", "11"}
				mockListCache.EXPECT().LRange(gomock.Any(), int64(0), repo.offSetLimit).Return(fakeIDs, nil).Times(1)

				// mock byID
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(redis.Nil).MaxTimes(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("error db")).MaxTimes(1)
				_, err := repo.Transactions(id)
				Expect(err).ToNot(BeNil())
				Expect(err.Error()).Should(Equal("error db"))
			})
			It("Should execute byCustomerIDDB if Llen return 0 items adn then set ids on cache", func() {
				mockListCache.EXPECT().LLen(gomock.Any()).Return(int64(0), nil).Times(1)
				// mock byCustomerIDDB
				mockDB.EXPECT().Query(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).Times(1)
				_, err := repo.Transactions(id)
				Expect(err).To(BeNil())
			})
			It("Should execute byCustomerIDDB and return error if it happens", func() {
				mockListCache.EXPECT().LLen(gomock.Any()).Return(int64(0), nil).Times(1)
				// mock byCustomerIDDB
				mockDB.EXPECT().Query(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("error")).Times(1)
				_, err := repo.Transactions(id)
				Expect(err).ToNot(BeNil())
			})
		})
		Context("WithOffSet", func() {
			It("Should have default offset  limit of 30", func() {
				Expect(repo.offSetLimit).Should(Equal(int64(30)))
			})

			It("Should alter limit offset", func() {
				opt := WithOffSet(10)
				opt(&repo)
				Expect(repo.offSetLimit).Should(Equal(int64(10)))
			})
		})
	})
})
