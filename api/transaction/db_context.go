package transaction

import (
	"database/sql"

	log "github.com/sirupsen/logrus"
	"gitlab.com/kauehmoreno/nubank-test/api/cache"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

// Transference all implementation method
type Transference interface {
	Create(tr *Transaction, rules ...Rules) error
	Transactions(customerID string) ([]Transaction, error)
	ByID(id string) (Transaction, error)
	ChangeStatus(id string, status string) error
}

// Options allow user to set options on manager
type Options func(r *repository)

// WithOffSet allow client to pick offset value on []Transaction
func WithOffSet(n int64) Options {
	return func(r *repository) {
		r.offSetLimit = n
	}
}

// NewManager expose to client a small method to access repository to interact with db and cache
func NewManager(opts ...Options) Transference {
	rp := repository{
		cacheRepo:   cache.GetInstance(),
		listRep:     cache.GetInstance(),
		database:    db.GetInstance(),
		offSetLimit: 30,
	}

	for _, opt := range opts {
		opt(&rp)
	}

	return rp
}

func saveDB(tr Transaction, database db.DB) error {
	query := `
	INSERT INTO transaction (
		id, created_at, scheduled, scheduled_at, 
		sender_acc_id, receiver_acc_id, status,
		amount, refund, external_transaction,
		bank_name, bank_number, bank_agency,
		user_name, user_identifier
	) VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,?, ?, ?, ?, ?, ?)
	`
	result, err := database.Exec(
		query, tr.ID, tr.CreatedAt, tr.Scheduled,
		tr.ScheduledAt, tr.SenderAccID, tr.ReceiverAccID,
		tr.Status, tr.Amount, tr.Refund, tr.ExternalTransaction,
		tr.Bank, tr.BankNumber,
		tr.BankAgency, tr.UserName,
		tr.Identifier,
	)

	if err != nil {
		return err
	}

	if affected, _ := result.RowsAffected(); affected == db.NoRowsAffected {
		return db.ErrNoAffectedRow
	}
	return nil
}

func changeStatusDB(id, status string, database db.DB) error {
	query := "UPDATE transaction SET status=? WHERE id=?"

	result, err := database.Exec(query, status, id)
	if err != nil {
		return err
	}

	if affected, _ := result.RowsAffected(); affected == db.NoRowsAffected {
		return db.ErrNoAffectedRow
	}
	return nil
}

func getTransactionDB(id string, database db.DB) (Transaction, error) {
	query := `
	SELECT id, created_at, scheduled, scheduled_at, 
	sender_acc_id, receiver_acc_id, status,
	amount, refund, external_transaction,
	bank_name, bank_number, bank_agency,
	user_name, user_identifier 
	FROM transaction WHERE id=?
	`
	var tr Transaction
	if err := database.Get(&tr, query, id); err != nil {
		return tr, err
	}
	return tr, nil
}

func byCustomerIDDB(customerID string, database db.DB) ([]Transaction, error) {
	query := `
	SELECT id, created_at, scheduled, scheduled_at, 
	sender_acc_id, receiver_acc_id, status,
	amount, refund, external_transaction,
	bank_name, bank_number, bank_agency,
	user_name, user_identifier
	FROM transaction WHERE sender_acc_id=? OR receiver_acc_id=?
	`
	trs := []Transaction{}
	if err := database.Query(&trs, query, customerID, customerID); err != nil {
		return trs, err
	}
	return trs, nil
}

type repository struct {
	cacheRepo   cache.Cache
	listRep     cache.ListCache
	database    db.DB
	offSetLimit int64
}

// Create create transaction based on rules it could be many of than
// check for enoughbalance, it could check customer existency in system
// for test purpose it will check only if sender has enoug balance to operate
func (r repository) Create(tr *Transaction, rules ...Rules) error {
	for _, rule := range rules {
		if err := rule(tr); err != nil {
			return err
		}
	}

	if err := saveDB(*tr, r.database); err != nil {
		return err
	}
	defer postCreate(*tr, r)
	return nil
}

func (r repository) ByID(id string) (Transaction, error) {
	var (
		tr  Transaction
		err error
	)
	key := cache.FormatKey(cache.KeyTransaction, id)
	if err = r.cacheRepo.Get(key, &tr); err != nil {
		tr, err = getTransactionDB(id, r.database)
		if err != nil {
			return tr, err
		}
		defer r.cacheRepo.Set(key, tr, cache.NoExpiration)
		return tr, err
	}
	return tr, nil
}

func (r repository) ChangeStatus(id string, status string) error {
	if err := changeStatusDB(id, status, r.database); err != nil {
		return err
	}
	key := cache.FormatKey(cache.KeyTransaction, id)
	var tr Transaction

	if err := r.cacheRepo.Get(key, &tr); err != nil {
		defer r.cacheRepo.Delete(key)
		return nil
	}
	tr.Status = status
	defer r.cacheRepo.Set(key, tr, cache.NoExpiration)
	return nil
}

// Transaction limit of 30
func (r repository) Transactions(accID string) ([]Transaction, error) {
	var (
		key string
	)

	transactions := []Transaction{}

	key = cache.FormatKey(cache.KeyUserTransactions, accID)
	qtd, err := r.listRep.LLen(key)
	if err != nil {
		return transactions, err
	}
	if qtd > 0 {
		ids, err := r.listRep.LRange(key, 0, r.offSetLimit)
		if err != nil {
			return transactions, err
		}
		return r.searchTransaction(ids...)
	}
	transactions, err = byCustomerIDDB(accID, r.database)
	if err != nil {
		return transactions, err
	}
	defer redoCacheIDs(r.listRep, transactions...)
	return transactions, nil
}

func (r repository) searchTransaction(ids ...string) ([]Transaction, error) {
	var transactions []Transaction
	for _, id := range ids {
		tr, err := r.ByID(id)
		if err != nil && err != sql.ErrNoRows {
			return transactions, err
		}
		if tr.ID != "" {
			transactions = append(transactions, tr)
		}
	}
	return transactions, nil
}

func redoCacheIDs(r cache.ListCache, trs ...Transaction) {
	var key string
	for _, tr := range trs {
		key = cache.FormatKey(cache.KeyUserTransactions, tr.SenderAccID)
		r.LPush(key, tr.ID)
		key = cache.FormatKey(cache.KeyUserTransactions, tr.ReceiverAccID)
		r.LPush(key, tr.ID)
	}
}

func postCreate(tr Transaction, r repository) {
	logger := log.Fields{"transaction": tr.ID, "sender": tr.SenderAccID, "receiver": tr.ReceiverAccID}
	key := cache.FormatKey(cache.KeyTransaction, tr.ID)
	if err := r.cacheRepo.Set(key, tr, cache.NoExpiration); err != nil {
		logger["error"] = err
		log.WithFields(logger).Error("Erro on set cache on transaction")
		return
	}

	key = cache.FormatKey(cache.KeyUserTransactions, tr.SenderAccID)
	if err := r.listRep.LPush(key, tr.ID); err != nil {
		logger["error"] = err
		log.WithFields(logger).Error("Erro on set list of transaction from a customer on cache")
		return
	}

	key = cache.FormatKey(cache.KeyUserTransactions, tr.ReceiverAccID)
	if err := r.listRep.LPush(key, tr.ID); err != nil {
		logger["error"] = err
		log.WithFields(logger).Error("Erro on set list of transaction from a customer on cache")
		return
	}

}
