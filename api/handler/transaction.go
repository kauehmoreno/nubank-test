package handler

import (
	"database/sql"
	"net/http"

	"gitlab.com/kauehmoreno/nubank-test/api/customer"
	"gitlab.com/kauehmoreno/nubank-test/api/queue"

	"gitlab.com/kauehmoreno/nubank-test/api/account"

	log "github.com/sirupsen/logrus"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/kauehmoreno/nubank-test/api/transaction"
)

// TransactionHandler struct responsable to deal with all transactions into system... both internal and external ones
type TransactionHandler struct {
	transaction.Transference
}

// NewTransactionHandler its used on router and pass manager already to struct to manipulate interaction with package easily
func NewTransactionHandler() TransactionHandler {
	return TransactionHandler{
		transaction.NewManager(),
	}
}

// InternalTransaction handle transactions between customer
func (t TransactionHandler) InternalTransaction(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	var body transaction.InternalTransactionPayload
	defer req.Body.Close()
	if err := UnMarshal(req.Body, &body); err != nil {
		log.Errorf("Erro on unmarshal request body: %s", err)
		writeErrorResponse(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	tr := transaction.New(body.SenderAccID, body.ReceiverAccID, transaction.WithAmount(body.Amount))
	erro := t.Create(&tr, transaction.CheckReceiverAndSender(), transaction.EnoughBalance(account.NewManager()))
	if erro != nil {
		if erro == transaction.ErrNoEnoughBalance {
			log.Errorf("Not enough balance from user %s - amount transference %f", tr.SenderAccID, tr.Amount)
			writeErrorResponse(w, "Not enough balance to finish transaction", http.StatusNotAcceptable)
			return
		}
		if erro == transaction.ErrConflictAccountID {
			log.Errorf("Customer %s is trying to send money to same account amount %f", tr.SenderAccID, tr.Amount)
			writeErrorResponse(w, "Conflit accountID", http.StatusConflict)
			return
		}
		log.Errorf("Erro on create transaction from %s to %s error %v", tr.SenderAccID, tr.ReceiverAccID, erro)
		writeErrorResponse(w, "Something bad happens please try again later", http.StatusInternalServerError)
		return
	}

	q := queue.GetInstance()
	defer q.Push(queue.TrasanctionCreated, tr)

	witeSuccessResponse(w, "Transaction was successfull processed", http.StatusOK)
	return
}

// ExternalTransactionIn of a customer receiving  money from external bank
func (t TransactionHandler) ExternalTransactionIn(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	var externalTr transaction.OutTrasanction

	defer req.Body.Close()

	if err := UnMarshal(req.Body, &externalTr); err != nil {
		log.Errorf("Erro on unmarshal request body: %s", err)
		writeErrorResponse(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	tr := transaction.New("", "", transaction.WithAmount(externalTr.Amount), transaction.WithExternalTransaction(externalTr.From))
	if err := t.Create(&tr, transaction.WithInsideCustomerValidation(true, externalTr.To)); err != nil {
		if err == customer.ErrCustomerDoesNotExist {
			writeErrorResponse(w, "Customer not found", http.StatusNotFound)
			return
		}
		log.Errorf("Error on create transaction something bad happens %v", err)
		writeErrorResponse(w, "Something bad happens please try again later", http.StatusInternalServerError)
		return
	}
	q := queue.GetInstance()
	if err := q.Push(queue.TrasanctionCreated, tr); err != nil {
		log.Errorf("Erro on queue transaction created %v", err)
	}

	witeSuccessResponse(w, "Transaction was successfull processed", http.StatusOK)
	return
}

// ExternalTransactionOut of a customer sending  money to external bank
func (t TransactionHandler) ExternalTransactionOut(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	var externalTr transaction.OutTrasanction

	if err := UnMarshal(req.Body, &externalTr); err != nil {
		log.Errorf("Erro on unmarshal request body: %s", err)
		writeErrorResponse(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	tr := transaction.New("", "", transaction.WithAmount(externalTr.Amount), transaction.WithExternalTransaction(externalTr.To))
	erro := t.Create(&tr, transaction.WithInsideCustomerValidation(false, externalTr.From), transaction.EnoughBalance(account.NewManager()))
	if erro != nil {
		if erro == transaction.ErrNoEnoughBalance {
			log.Errorf("Not enough balance from user %s - amount transference %f", tr.SenderAccID, tr.Amount)
			writeErrorResponse(w, "Not enough balance to finish transaction", http.StatusNotAcceptable)
			return
		}
		if erro == customer.ErrCustomerDoesNotExist {
			writeErrorResponse(w, "User does not have account ", http.StatusNotFound)
			return
		}

		log.Errorf("Error on create transaction something bad happens %v", erro)
		writeErrorResponse(w, "Something bad happens please try again later", http.StatusInternalServerError)
		return
	}

	q := queue.GetInstance()
	defer q.Push(queue.TrasanctionCreated, tr)

	witeSuccessResponse(w, "Transaction was successfull processed", http.StatusOK)
	return

}

// CustomerTransactions returns last 30 transaction from a customer
func (t TransactionHandler) CustomerTransactions(w http.ResponseWriter, req *http.Request, param httprouter.Params) {
	id := param.ByName("customerID")

	cm := customer.NewManager()
	var c customer.Customer
	if err := cm.ByID(&c, id); err != nil {
		if err == customer.ErrCustomerDoesNotExist {
			writeErrorResponse(w, "Customer not found", http.StatusNotFound)
			return
		}
		log.Errorf("Error on find transactions %s  something bad happens %v", id, err)
		writeErrorResponse(w, "Something bad happen please try again later", http.StatusInternalServerError)
		return

	}

	tr, err := t.Transactions(c.AccountID)
	if err != nil {
		if err == sql.ErrNoRows {
			writeResponse(w, tr, http.StatusOK)
			return
		}
		log.Errorf("Erro on get users %s transactions - error: %v", id, err)
		writeErrorResponse(w, "Something bad happen please try again later", http.StatusInternalServerError)
		return
	}

	writeResponse(w, tr, http.StatusOK)
	return
}
