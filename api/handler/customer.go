package handler

import (
	"net/http"

	"gitlab.com/kauehmoreno/nubank-test/api/db"

	"gitlab.com/kauehmoreno/nubank-test/api/queue"

	log "github.com/sirupsen/logrus"
	"gitlab.com/kauehmoreno/nubank-test/api/customer"

	"github.com/julienschmidt/httprouter"
)

// CustomerHandler embeded manager to easily perform operations
type CustomerHandler struct {
	customer.Manager
}

// NewCustomerHandler is used on router to have access of CustomerHandler
func NewCustomerHandler() CustomerHandler {
	return CustomerHandler{
		customer.NewManager(),
	}
}

// RegisterCustomer register a client into our system
func (c CustomerHandler) RegisterCustomer(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	var customer customer.Customer
	defer req.Body.Close()
	if err := UnMarshal(req.Body, &customer); err != nil {
		log.Errorf("Erro on unMarshal request body: %s", err)
		writeErrorResponse(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	if erro := c.Save(&customer); erro != nil {
		if erro == db.ErrDuplicated {
			writeErrorResponse(w, "Customer already exists", http.StatusConflict)
			return
		}
		log.WithFields(log.Fields{"error": erro, "customer_email": customer.Email}).Error("Error on save customer")
		writeErrorResponse(w, "Could not register customer ", http.StatusNotAcceptable)
		return
	}
	q := queue.GetInstance()
	defer q.Push(queue.CreateAccount, customer)
	writeResponse(w, customer, http.StatusCreated)
	return
}

// CustomerInfo return all info from a user based on id
func (c CustomerHandler) CustomerInfo(w http.ResponseWriter, req *http.Request, param httprouter.Params) {
	customerID := param.ByName("customerID")
	var cli customer.Customer
	if err := c.ByID(&cli, customerID); err != nil {
		if err == customer.ErrCustomerDoesNotExist {
			writeErrorResponse(w, "User not found", http.StatusNotFound)
			return
		}
		log.Errorf("error on get user byID %s something happen %v", customerID, err)
		writeErrorResponse(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	writeResponse(w, cli, http.StatusOK)
	return
}
