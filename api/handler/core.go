package handler

import (
	"encoding/json"
	"io"
)

// UnMarshal simplify use of all handlers to decode json body
// it a simple wrapper to get body read and unmarshal to value expected type
func UnMarshal(body io.Reader, value interface{}) error {
	decoder := json.NewDecoder(body)
	return decoder.Decode(&value)
}
