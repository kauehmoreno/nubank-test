package handler

import (
	"net/http"

	"gitlab.com/kauehmoreno/nubank-test/api/customer"

	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kauehmoreno/nubank-test/api/account"
)

// TransactionHandler struct responsable to deal with all transactions into system... both internal and external ones
type AccountHandler struct {
	account.Acc
}

// NewTransactionHandler its used on router and pass manager already to struct to manipulate interaction with package easily
func NewAccountHandler() AccountHandler {
	return AccountHandler{
		account.NewManager(),
	}
}

func (t AccountHandler) CostumerBalance(w http.ResponseWriter, req *http.Request, param httprouter.Params) {
	var c customer.Customer

	id := param.ByName("customerID")

	cm := customer.NewManager()
	if err := cm.ByID(&c, id); err != nil {
		if err == customer.ErrCustomerDoesNotExist {
			writeErrorResponse(w, "Customer not found", http.StatusNotFound)
			return
		}
		log.Errorf("Error on look customer ByID to check its balance %s  something bad happens %v", id, err)
		writeErrorResponse(w, "Something bad happen please try again later", http.StatusInternalServerError)
		return
	}

	am := account.NewManager()

	acc, erro := am.Account(c.AccountID)
	if erro != nil {
		log.Errorf("Error on find customer %s account %s something bad happens %v", id, c.AccountID, erro)
		writeErrorResponse(w, "Something bad happen please try again later", http.StatusInternalServerError)
		return
	}

	response := struct {
		Balance    float64 `json:"balance"`
		Agency     uint16  `json:"agency"`
		Number     uint32  `json:"number"`
		CustomerID string  `json:"customer_id"`
	}{acc.GetBalance(), acc.GetAgency(), acc.GetNumber(), c.ID}

	writeResponse(w, response, http.StatusOK)
	return
}
