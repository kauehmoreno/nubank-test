package handler

import (
	"encoding/json"
	"net/http"
)

type apiResponse struct {
	Message string `json:"message"`
	Status  int    `json:"status"`
}

// WriteErrorResponse is used in case of error catched on handler and response write to user
func writeErrorResponse(w http.ResponseWriter, message string, statusCode int) {
	writeResponse(w, apiResponse{
		Message: message,
		Status:  statusCode,
	}, statusCode)
}

// WriteSuccessResponse ...
func witeSuccessResponse(w http.ResponseWriter, message string, statusCode int) {
	writeResponse(w, apiResponse{
		Message: message,
		Status:  statusCode,
	}, statusCode)
}

// WriteResponse is used to receive any interface and encoded to json
func writeResponse(w http.ResponseWriter, data interface{}, statusCode int) {
	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.Header().Set("Vary", "Accept-Encoding")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(&data)
}

// WriteCachedResponse is used to set max-age cache on header
// those case are used to Nginx identify and cache result to client
func writeCachedResponse(w http.ResponseWriter, data interface{}, statusCode int, maxAge string) {
	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.Header().Set("Vary", "Accept-Encoding")
	w.Header().Set("Cache-Control", maxAge)
	w.WriteHeader(statusCode)

	json.NewEncoder(w).Encode(&data)
}

// writeHealthcheckError os used on healtcheck routers
func writeHealthcheckError(w http.ResponseWriter, msg string, statusCode int) {
	w.Header().Set("Content-Type", "text/plain;charset=UTF-8")
	w.WriteHeader(statusCode)
	w.Write([]byte(msg))
}
