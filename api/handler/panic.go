package handler

import (
	"net/http"

	log "github.com/sirupsen/logrus"
)

// PanicCustomRecovery catches all panic on API and keep it up
func PanicCustomRecovery(w http.ResponseWriter, req *http.Request, err interface{}) {
	log.WithFields(log.Fields{
		"error":  err,
		"method": "handler.PanicCustomRecovery",
	}).Error("Panic Error handler - Something got wrong and was not expected")
	writeErrorResponse(w, "Something got wrong - try it later", http.StatusInternalServerError)
}
