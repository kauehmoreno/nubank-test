// +test integration

package handler_test

import (
	"fmt"
	"math/rand"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/kauehmoreno/nubank-test/api"
	"gitlab.com/kauehmoreno/nubank-test/api/account"
	"gitlab.com/kauehmoreno/nubank-test/api/customer"
	"gitlab.com/kauehmoreno/nubank-test/api/handler"
	"gitlab.com/kauehmoreno/nubank-test/api/settings"
	"gitlab.com/kauehmoreno/nubank-test/api/transaction"
)

var (
	host                      = settings.Get().Host
	createcustumerURL         = fmt.Sprintf("%s/customer/register", host)
	customerInfoURL           = fmt.Sprintf("%s/customer/detail/", host)
	externalTransactionURLIn  = fmt.Sprintf("%s/transaction/in", host)
	externalTransactionURLOut = fmt.Sprintf("%s/transaction/out", host)
	transactionURL            = fmt.Sprintf("%s/transaction", host)
	accountBalanceURL         = fmt.Sprintf("%s/account/", host)
	accountListTransaction    = fmt.Sprintf("%s/transactions/", host)
	healthCheckURL            = fmt.Sprintf("%s/healthcheck", settings.Get().Host)
	header                    = map[string]string{
		"Content-Type": "application/json",
	}
)

type HandlerIntegrateTestSuite struct {
	suite.Suite
}

func TestHandlerTestSuiteCase(t *testing.T) {
	if err := checkWithApiIsUp(); err != nil {
		t.Skipf("Api is not up - please execute make run to run integration tests  err: %v", err)
	}
	fmt.Println("GO GRAB SOME COFFE - IT MIGHT TAKE A WHILE ;)")
	suite.Run(t, new(HandlerIntegrateTestSuite))
}

func (suite *HandlerIntegrateTestSuite) SetupTest() {
	fmt.Println("Waiting for next test setup ...")
	time.Sleep(time.Second * 30)
}

func (suite *HandlerIntegrateTestSuite) TestGetCustomerAfterCreate() {
	customers, err := generateCustomers()
	suite.Require().NoError(err)
	suite.Require().Len(customers, 2, "Should have two customers created")

	resp, err := getCustomer(customers[0].ID)
	suite.Require().NoError(err)
	suite.Require().Equal(resp.StatusCode, http.StatusOK, "Should responde statusCode 200")

	var c customer.Customer
	handler.UnMarshal(resp.Body, &c)
	defer resp.Body.Close()

	suite.Require().Equal(c.ID, customers[0].ID, "Should match accountID")
	suite.Require().Equal(c.Email, customers[0].Email, "Should match accountID")
	suite.Require().Equal(c.AccountID, customers[0].AccountID, "Should match accountID")

}

func (suite *HandlerIntegrateTestSuite) TestCreateExternalTransaction() {
	customers, err := generateCustomers()
	suite.Require().NoError(err)
	suite.Require().Len(customers, 2, "Should have two customers created")

	resp, err := executeExternalTransactionIn(customers[0], float64(100.10))
	suite.Require().NoError(err)
	suite.Require().Equal(resp.StatusCode, http.StatusOK, "Should return statuscode 200")
}

func (suite *HandlerIntegrateTestSuite) TestTransactionInternalBetweenCustomers() {
	customers, err := generateCustomers()
	suite.Require().NoError(err)
	suite.Require().Len(customers, 2, "Should have two customers created")

	executeExternalTransactionIn(customers[0], float64(100.10))
	time.Sleep(time.Second * 60)
	resp, err := executeInterTransaction(customers[0].AccountID, customers[1].AccountID, float64(50.32))
	suite.Require().NoError(err)
	suite.Require().Equal(resp.StatusCode, http.StatusOK, "Should return statuscode 200")
}

func (suite *HandlerIntegrateTestSuite) TestCheckBalanceAfterOperationMustBeUpdated() {
	customers, err := generateCustomers()
	suite.Require().NoError(err)
	suite.Require().Len(customers, 2, "Should have two customers created")

	executeExternalTransactionIn(customers[0], float64(100.10))
	time.Sleep(time.Second * 60)
	executeInterTransaction(customers[0].AccountID, customers[1].AccountID, float64(50.32))
	time.Sleep(time.Second * 60)
	resp, _ := checkCustomerBalance(customers[0].ID)
	acc := struct {
		Balance    float64 `json:"balance"`
		Agency     uint16  `json:"agency"`
		Number     uint32  `json:"number"`
		CustomerID string  `json:"customer_id"`
	}{}

	handler.UnMarshal(resp.Body, &acc)
	defer resp.Body.Close()

	suite.Require().NotEqual(acc.Balance, float64(100.10), "Should be less than 100.10")
	suite.Equal(acc.Balance, float64(49.78), "Should be equal to 49.78")
	suite.Require().Equal(acc.CustomerID, customers[0].ID, "Should be the same customer")
}

func (suite *HandlerIntegrateTestSuite) TestExternalTransferOut() {

	customers, err := generateCustomers()
	suite.Require().NoError(err)
	suite.Require().Len(customers, 2, "Should have two customers created")

	executeExternalTransactionIn(customers[0], float64(100.10))
	time.Sleep(time.Second * 60)

	resp, err := executeExternalTransferOut(customers[0], float64(30.10))
	suite.Require().NoError(err)
	suite.Require().Equal(resp.StatusCode, http.StatusOK, "Should be ok")
	time.Sleep(time.Second * 60)
	defer resp.Body.Close()

	data, _ := checkCustomerBalance(customers[0].ID)
	acc := struct {
		Balance    float64 `json:"balance"`
		Agency     uint16  `json:"agency"`
		Number     uint32  `json:"number"`
		CustomerID string  `json:"customer_id"`
	}{}

	handler.UnMarshal(data.Body, &acc)
	defer data.Body.Close()
	suite.Require().Equal(acc.Balance, float64(70), "Should be equal to 70")
	suite.Require().Equal(acc.CustomerID, customers[0].ID, "Should be the same customer")
}

func (suite *HandlerIntegrateTestSuite) TestShouldReturnNotEnoughFoundToTransaction() {
	customers, err := generateCustomers()
	suite.Require().NoError(err)
	suite.Require().Len(customers, 2, "Should have two customers created")
	time.Sleep(time.Second * 30)
	resp, err := executeInterTransaction(customers[0].AccountID, customers[1].AccountID, float64(78.98))
	suite.Require().Equal(resp.StatusCode, http.StatusNotAcceptable, "Should return 406")
	suite.Require().NoError(err)
}

func (suite *HandlerIntegrateTestSuite) TestShouldConflictToTransactionToSameCustomerAccount() {
	customers, err := generateCustomers()
	suite.Require().NoError(err)
	suite.Require().Len(customers, 2, "Should have two customers created")
	time.Sleep(time.Second * 30)
	resp, err := executeInterTransaction(customers[0].AccountID, customers[0].AccountID, float64(78.98))
	suite.Require().Equal(resp.StatusCode, http.StatusConflict, "Should return 409")
	suite.Require().NoError(err)
}

// generateCustomers always generate two users
// changing their cpf to not be equals
func generateCustomers() ([]customer.Customer, error) {
	rand.Seed(time.Now().UnixNano())
	num := rand.Intn(1000)
	customers := []customer.Customer{}
	users := []struct {
		Name  string `json:"name"`
		Email string `json:"email"`
		Cpf   string `json:"cpf"`
	}{
		{"Marcus Migule", fmt.Sprintf("marcus_miguel%d@teste.com", num), fmt.Sprintf("052.%d.111-11", num)},
		{"Artur Pereira", fmt.Sprintf("artur%d@teste.com", num), fmt.Sprintf("055.%d.123-22", num)},
	}

	for _, user := range users {
		var cli customer.Customer
		resp, err := api.Post(createcustumerURL, header, user)
		if err != nil {
			return customers, err
		}
		defer resp.Body.Close()
		if err = handler.UnMarshal(resp.Body, &cli); err != nil {
			return customers, err
		}
		customers = append(customers, cli)
	}
	return customers, nil
}

func checkWithApiIsUp() error {
	if _, err := api.Get(healthCheckURL, header); err != nil {
		return err
	}
	return nil
}

func generateExternalTransactionPayloadIn(cpf, name string, amount float64) transaction.OutTrasanction {
	return transaction.OutTrasanction{
		From: account.ExternalAccountInfo{
			Bank:       "itau",
			BankNumber: 26321,
			BankAgency: 2222,
			UserName:   "Roberto Pereira Filho",
			Identifier: "099.099.099-99",
		},
		To: account.ExternalAccountInfo{
			Bank:       "nubank",
			UserName:   name,
			BankAgency: 1010,
			BankNumber: 0,
			Identifier: cpf,
		},
		Amount: amount,
	}
}

func generateExternalTransactionPayloadOut(cpf, name string, amount float64) transaction.OutTrasanction {
	return transaction.OutTrasanction{
		From: account.ExternalAccountInfo{
			Bank:       "nubank",
			UserName:   name,
			BankAgency: 1010,
			BankNumber: 0,
			Identifier: cpf,
		},
		To: account.ExternalAccountInfo{
			Bank:       "itau",
			BankNumber: 26321,
			BankAgency: 2222,
			UserName:   "Roberto Pereira Filho",
			Identifier: "099.099.099-99",
		},
		Amount: amount,
	}
}

func generateInternalTransactionPayload(sender, receiver string, amount float64) transaction.InternalTransactionPayload {
	return transaction.InternalTransactionPayload{
		SenderAccID:   sender,
		ReceiverAccID: receiver,
		Amount:        amount,
	}
}

func executeExternalTransactionIn(c customer.Customer, amount float64) (*http.Response, error) {
	payload := generateExternalTransactionPayloadIn(c.Cpf, c.Name, amount)
	return api.Post(externalTransactionURLIn, header, payload)
}

func executeInterTransaction(accID1, accID2 string, amount float64) (*http.Response, error) {
	payload := generateInternalTransactionPayload(accID1, accID2, amount)
	return api.Post(transactionURL, header, payload)
}

func getCustomer(customerID string) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", customerInfoURL, customerID)
	return api.Get(url, header)
}

func checkCustomerBalance(customerID string) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", accountBalanceURL, customerID)
	return api.Get(url, header)
}

func executeExternalTransferOut(c customer.Customer, amount float64) (*http.Response, error) {
	payload := generateExternalTransactionPayloadOut(c.Cpf, c.Name, amount)
	return api.Post(externalTransactionURLOut, header, payload)
}

func getCustomerTransactions(customerID string) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", accountListTransaction, customerID)
	return api.Get(url, header)
}
