package handler

import (
	"net/http"

	"gitlab.com/kauehmoreno/nubank-test/api/queue"

	"github.com/sirupsen/logrus"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/kauehmoreno/nubank-test/api/cache"
)

// HealthCheckHandler basic healthcheck api
func HealthCheckHandler(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	writeHealthcheckError(w, "WORKING", http.StatusOK)
}

func CacheHealthCheckHandler(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	c := cache.GetInstance()
	if err := c.Ping(); err != nil {
		logrus.Errorf("Redis cluster not working: %v", err)
		writeHealthcheckError(w, "NOT WORKING", http.StatusInternalServerError)
		return
	}
	writeHealthcheckError(w, "WORKING", http.StatusOK)
}

func QueueHealthCheckHandler(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	q := queue.GetInstance()
	if err := q.Ping(); err != nil {
		logrus.Errorf("Queue not working: %v", err)
		writeHealthcheckError(w, "NOT WORKING", http.StatusInternalServerError)
		return
	}
	writeHealthcheckError(w, "WORKING", http.StatusOK)

}
