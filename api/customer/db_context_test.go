// +test unit

package customer

import (
	"errors"
	"time"

	"github.com/go-redis/redis"

	"gitlab.com/kauehmoreno/nubank-test/api/cache"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

var _ = Describe("DBContext", func() {

	var (
		mockCtrl  *gomock.Controller
		mockCache *cache.MockCache
		mockDB    *db.MockDB
		u         = New("teste", "teste@email.com", "999.999.999-99")
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockDB = db.NewMockDB(mockCtrl)
		mockCache = cache.NewMockCache(mockCtrl)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Describe("Repository", func() {
		Context("ByID", func() {
			It("Should return error and convertion should be respect if so", func() {
				id := db.GenerateUUID()
				mockDB.EXPECT().Get(&u, gomock.Any(), gomock.Any()).Return(nil)
				repo := dbRepository{
					mockDB,
				}
				err := repo.byID(&u, id)
				Expect(err).To(BeNil())
				Expect(u.ID).ShouldNot(BeEmpty())
			})
			It("Should return error whenever any operation on db throw it up", func() {
				id := db.GenerateUUID()
				mockDB.EXPECT().Get(&u, gomock.Any(), gomock.Any()).Return(errors.New("Erro timeout connection"))
				repo := dbRepository{
					mockDB,
				}
				err := repo.byID(&u, id)
				Expect(err).ShouldNot(BeNil())
				Expect(err.Error()).Should(Equal("Erro timeout connection"))
			})
		})
		Context("ByCPF", func() {
			It("Should return error and convertion should be respect if so", func() {
				mockDB.EXPECT().Get(&u, gomock.Any(), gomock.Any()).Return(nil)
				repo := dbRepository{
					mockDB,
				}
				err := repo.byCPF(&u, u.Cpf)
				Expect(err).To(BeNil())
				Expect(u.ID).ShouldNot(BeEmpty())
			})
			It("Should return error whenever any operation on db throw it up", func() {
				mockDB.EXPECT().Get(&u, gomock.Any(), gomock.Any()).Return(errors.New("Erro timeout connection"))
				repo := dbRepository{
					mockDB,
				}
				err := repo.byCPF(&u, u.Cpf)
				Expect(err).ShouldNot(BeNil())
				Expect(err.Error()).Should(Equal("Erro timeout connection"))
			})
		})
		Context("Save", func() {
			It("Should execute without error", func() {
				result := db.MockSQLResult{
					Error:        errors.New("Error on exec query"),
					LastInsertID: 0,
					RowAffected:  0,
				}
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(result, nil)
				repo := dbRepository{
					mockDB,
				}
				err := repo.save(&u)
				Expect(err).ToNot(BeNil())
			})

			It("Should return error when no rows are affected ", func() {
				result := db.MockSQLResult{
					Error:        nil,
					LastInsertID: 0,
					RowAffected:  0,
				}
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(result, nil)
				repo := dbRepository{
					mockDB,
				}
				err := repo.save(&u)
				Expect(err).ToNot(BeNil())
				Expect(err).Should(Equal(db.ErrNoAffectedRow))
			})
			It("Should retur no error when operation is fine", func() {
				result := db.MockSQLResult{
					Error:        nil,
					LastInsertID: 0,
					RowAffected:  1,
				}
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(result, nil)
				repo := dbRepository{
					mockDB,
				}
				err := repo.save(&u)
				Expect(err).To(BeNil())
			})
		})
	})

	Describe("Cache Repository", func() {

		Context("ByID", func() {
			It("Should execute cache only whenever it find a key", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}
				id := db.GenerateUUID()
				key := cache.FormatKey(cache.KeyUser, id)
				mockCache.EXPECT().Get(key, &u).Return(nil)
				err := repo.ByID(&u, id)
				Expect(err).Should(BeNil())
			})
			It("Shoul match inside method with the given one params on build key to access cache", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}
				id := db.GenerateUUID()
				key := cache.FormatKey(cache.KeyUser, id)
				mockCache.EXPECT().Get(key, &u).Do(func(key string, u *Customer) {
					expected := cache.FormatKey(cache.KeyUser, id)
					Expect(key).Should(Equal(expected))
				})
				repo.ByID(&u, id)
			})
			It("Should execute database when cache returns error and then try to set back on cache", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}
				id := db.GenerateUUID()
				key := cache.FormatKey(cache.KeyUser, id)
				mockCache.EXPECT().Get(key, &u).Return(errors.New("Some error"))
				mockCache.EXPECT().Set(key, &u, cache.NoExpiration).Return(nil).Times(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).Times(1)
				err := repo.ByID(&u, id)
				Expect(err).To(BeNil())
			})
			It("Should respect key on set and reuse the same one as well as time cache must be equal to zero", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}
				id := db.GenerateUUID()
				key := cache.FormatKey(cache.KeyUser, id)
				mockCache.EXPECT().Get(key, &u).Return(errors.New("Some error"))
				mockCache.EXPECT().Set(key, &u, cache.NoExpiration).DoAndReturn(func(key string, u *Customer, t time.Duration) {
					expected := cache.FormatKey(cache.KeyUser, id)
					Expect(key).Should(Equal(expected))
					Expect(u.Email).Should(Equal("teste@email.com"))
					Expect(t).Should(Equal(time.Duration(0)))
				}).Return(nil).Times(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).Times(1)
				repo.ByID(&u, id)
			})
			It("Should not execute cache to set value if db returns error", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}
				id := db.GenerateUUID()
				key := cache.FormatKey(cache.KeyUser, id)
				mockCache.EXPECT().Get(key, &u).Return(redis.Nil)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("error")).Times(1)
				mockCache.EXPECT().Set(key, u, cache.NoExpiration).Times(0)
				repo.ByID(&u, id)
			})
			It("Should execute db if redis reutrn nil", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}
				id := db.GenerateUUID()
				key := cache.FormatKey(cache.KeyUser, id)
				mockCache.EXPECT().Get(key, &u).Return(redis.Nil)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).Times(1)
				mockCache.EXPECT().Set(key, &u, cache.NoExpiration).Times(1)
				repo.ByID(&u, id)
			})
		})
		Context("ByCPF", func() {
			It("Should return from cache only if find key - it must execute two gets on cache", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil).Times(2)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).Times(0)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), gomock.Any()).Times(0)
				err := repo.ByCPF(&u, u.Cpf)
				Expect(err).To(BeNil())
			})
			It("Should execute db in case of error or from cache adn then execute two times on cache with set", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(redis.Nil).Times(2)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), gomock.Any()).Times(2)
				repo.ByCPF(&u, u.Cpf)
			})
			It("Should not excecute set on db error", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(redis.Nil).Times(2)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("error db")).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), gomock.Any()).Times(0)
				err := repo.ByCPF(&u, u.Cpf)
				Expect(err).ToNot(BeNil())
			})
		})
		Context("Save", func() {
			It("In case of error should not go forward to save execution returns error right away", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}

				result := db.MockSQLResult{
					Error:        nil,
					LastInsertID: 0,
					RowAffected:  0,
				}
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(result, errors.New("error db")).Times(1)
				err := repo.Save(&u)
				Expect(err).ToNot(BeNil())
			})
			It("In case of success save on db it will set right after on cache", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}

				result := db.MockSQLResult{
					Error:        nil,
					LastInsertID: 0,
					RowAffected:  1,
				}
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(result, nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), gomock.Any()).Times(1)
				err := repo.Save(&u)
				Expect(err).To(BeNil())
			})
			It("Should have all params on set correct - user should be modified by db and key should be correct", func() {
				repo := repository{
					cacheRepo: mockCache,
					db: dbRepository{
						mockDB,
					},
				}

				result := db.MockSQLResult{
					Error:        nil,
					LastInsertID: 0,
					RowAffected:  1,
				}
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(result, nil).Do(func(query string, params ...interface{}) {
					Expect(query).ShouldNot(BeEmpty())
				}).Times(1)

				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), gomock.Any()).Do(func(key string, u *Customer, exp time.Duration) {
					expectedKey := cache.FormatKey(cache.KeyUser, u.ID)
					Expect(exp).Should(Equal(time.Duration(0)))
					Expect(key).Should(Equal(expectedKey))
				}).Times(1)
				err := repo.Save(&u)
				Expect(err).To(BeNil())
			})
		})
	})
})
