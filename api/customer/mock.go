// Code generated by MockGen. DO NOT EDIT.
// Source: api/customer/db_context.go

// Package mock_customer is a generated GoMock package.
package customer

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockManager is a mock of Manager interface
type MockManager struct {
	ctrl     *gomock.Controller
	recorder *MockManagerMockRecorder
}

// MockManagerMockRecorder is the mock recorder for MockManager
type MockManagerMockRecorder struct {
	mock *MockManager
}

// NewMockManager creates a new mock instance
func NewMockManager(ctrl *gomock.Controller) *MockManager {
	mock := &MockManager{ctrl: ctrl}
	mock.recorder = &MockManagerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockManager) EXPECT() *MockManagerMockRecorder {
	return m.recorder
}

// ByID mocks base method
func (m *MockManager) ByID(u *Customer, id string) error {
	ret := m.ctrl.Call(m, "ByID", u, id)
	ret0, _ := ret[0].(error)
	return ret0
}

// ByID indicates an expected call of ByID
func (mr *MockManagerMockRecorder) ByID(u, id interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ByID", reflect.TypeOf((*MockManager)(nil).ByID), u, id)
}

// ByCPF mocks base method
func (m *MockManager) ByCPF(u *Customer, cpf string) error {
	ret := m.ctrl.Call(m, "ByCPF", u, cpf)
	ret0, _ := ret[0].(error)
	return ret0
}

// ByCPF indicates an expected call of ByCPF
func (mr *MockManagerMockRecorder) ByCPF(u, cpf interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ByCPF", reflect.TypeOf((*MockManager)(nil).ByCPF), u, cpf)
}

// Save mocks base method
func (m *MockManager) Save(u *Customer) error {
	ret := m.ctrl.Call(m, "Save", u)
	ret0, _ := ret[0].(error)
	return ret0
}

// Save indicates an expected call of Save
func (mr *MockManagerMockRecorder) Save(u interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Save", reflect.TypeOf((*MockManager)(nil).Save), u)
}
