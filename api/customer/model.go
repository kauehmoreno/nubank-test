package customer

import (
	"strconv"

	"github.com/cespare/xxhash"
	"gitlab.com/kauehmoreno/nubank-test/api/cache"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

// Customer is the representation of model entity
type Customer struct {
	ID        string `json:"id" db:"id"`
	AccountID string `json:"account_id" db:"account_id"`
	Name      string `json:"name" db:"name"`
	Email     string `json:"email" db:"email"`
	Cpf       string `json:"cpf" db:"cpf"`
}

// New return a new user based on both params
// simplyfing package api to client
func New(name, email, cpf string) Customer {
	id := db.GenerateUUID()
	accountID := GenerateAccountID(id, email)
	return Customer{
		ID:        id,
		AccountID: accountID,
		Name:      name,
		Email:     email,
		Cpf:       cpf,
	}
}

// GenerateAccountID based on id and email generate accountID
func GenerateAccountID(params ...string) string {
	result := cache.FormatKey(params[0], params[1:]...)
	return strconv.FormatUint(xxhash.Sum64String(result), 10)
}
