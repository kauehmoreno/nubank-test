// +test unit

package customer_test

import (
	"fmt"
	"strconv"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/kauehmoreno/nubank-test/api/customer"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

var _ = Describe("Package customer - model", func() {
	var (
		id, email string
	)

	BeforeSuite(func() {
		id = db.GenerateUUID()
		email = "teste@email.com"
	})

	Describe("GenerateAccountID", func() {
		Context("In case of success generation", func() {
			It("Should build a different expected output", func() {
				notExpected := fmt.Sprintf("%s:%s", id, email)
				result := customer.GenerateAccountID(id, email)
				Expect(result).ShouldNot(Equal(notExpected))
			})
			It("Should contains only numbers", func() {
				result := customer.GenerateAccountID(id, email)
				_, err := strconv.ParseUint(result, 10, 64)
				Expect(err).To(BeNil())
			})
		})
	})

	Describe("New customer", func() {
		Context("On New should generate ID and AccountID", func() {
			name := "teste Nome"
			It("Should have ID not empty", func() {
				u := customer.New(name, email, "999.999.999-99")
				Expect(u.ID).ShouldNot(BeEmpty())
			})
			It("Should have accountID not empty", func() {
				u := customer.New(name, email, "999.999.999-99")
				Expect(u.AccountID).ShouldNot(BeEmpty())
			})
		})
	})
})
