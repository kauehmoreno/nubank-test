package customer

import (
	"database/sql"
	"errors"

	"gitlab.com/kauehmoreno/nubank-test/api/cache"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

var (
	// ErrCustomerDoesNotExist when looking for a customer returns nothing
	ErrCustomerDoesNotExist = errors.New("Customer does not exist on system")
)

// Manager implement methods with basic interation
type Manager interface {
	ByID(u *Customer, id string) error
	ByCPF(u *Customer, cpf string) error
	Save(u *Customer) error
}

// NewManager return information of manager which knows about repository, so it able to interact with
// both cache and db
func NewManager() Manager {
	return repository{
		cache.GetInstance(),
		dbRepository{
			db.GetInstance(),
		},
	}
}

type dbRepository struct {
	db.DB
}

func (d dbRepository) byID(u *Customer, id string) error {
	query := "/*ByID.Customer */ SELECT id, account_id, name, email, cpf FROM customer WHERE id=?"
	if err := d.Get(u, query, id); err != nil {
		if err == sql.ErrNoRows {
			return ErrCustomerDoesNotExist
		}
		return err
	}
	if u.ID == "" {
		return ErrCustomerDoesNotExist
	}
	return nil
}

func (d dbRepository) byCPF(u *Customer, cpf string) error {
	query := "/*ByEmail */ SELECT id, account_id, name, email, cpf FROM customer WHERE cpf=?"
	if err := d.Get(u, query, cpf); err != nil {
		if err == sql.ErrNoRows {
			return ErrCustomerDoesNotExist
		}
		return err
	}

	if u.ID == "" {
		return ErrCustomerDoesNotExist
	}

	return nil
}

// Save a Customer creation on db and build cache strategy
func (d dbRepository) save(u *Customer) error {
	nCustomer := New(u.Name, u.Email, u.Cpf)
	u.ID = nCustomer.ID
	u.AccountID = nCustomer.AccountID

	query := "/* save.Customer */ INSERT INTO customer (id, account_id, name, email, cpf) VALUES ( ?, ?, ?, ?, ?)"
	result, err := d.Exec(query, u.ID, u.AccountID, u.Name, u.Email, u.Cpf)
	if err != nil {
		return err
	}
	if affected, _ := result.RowsAffected(); affected == db.NoRowsAffected {
		return db.ErrNoAffectedRow
	}
	return nil
}

type repository struct {
	cacheRepo cache.Cache
	db        dbRepository
}

func (r repository) ByID(u *Customer, id string) error {
	key := cache.FormatKey(cache.KeyUser, id)
	if err := r.cacheRepo.Get(key, u); err != nil {
		if erro := r.db.byID(u, id); erro != nil {
			return erro
		}
		defer r.cacheRepo.Set(key, u, cache.NoExpiration)
		return nil
	}
	return nil
}

func (r repository) ByCPF(u *Customer, cpf string) error {
	var (
		key         string
		CustomerKey string
	)
	key = cache.FormatKey(cache.KeyByCPF, cpf)
	r.cacheRepo.Get(key, &CustomerKey)
	if err := r.cacheRepo.Get(CustomerKey, u); err != nil {
		if err := r.db.byCPF(u, cpf); err != nil {
			return err
		}
		defer func() {
			r.cacheRepo.Set(CustomerKey, u, cache.NoExpiration)
			r.cacheRepo.Set(key, CustomerKey, cache.NoExpiration)
		}()
	}
	return nil
}

// Save a Customer creation
func (r repository) Save(u *Customer) error {
	if err := r.db.save(u); err != nil {
		return err
	}
	key := cache.FormatKey(cache.KeyUser, u.ID)
	defer r.cacheRepo.Set(key, u, cache.NoExpiration)
	return nil
}
