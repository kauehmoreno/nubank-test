package settings

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/viper"
)

var settingsMapper *Settings

func init() {

	var conf string
	if conf = os.Getenv("NUBANK_TESTE"); conf == "" {
		conf = "deploy/settings_local.json"
	}

	var (
		errCount int
		err      error
	)

	for errCount <= 4 {
		if err = tryReadInConfig(conf); err != nil {
			errCount++
			conf = fmt.Sprintf("../%s", conf)
			continue
		}
		break
	}
	if err != nil {
		log.Fatalf("Error on load setings %s - it might not exists on path defined: %s", err, conf)
	}

	// setupOverrides

	settingsMapper = &Settings{}
	if err := viper.Unmarshal(settingsMapper); err != nil {
		log.Fatalf("Erro on unmarshal settings %s", err)
	}

	// add port on host
}

func tryReadInConfig(conf string) error {
	viper.SetConfigFile(conf)
	return viper.ReadInConfig()
}

// Get returns settings to allow interaction with config
func Get() *Settings {
	return settingsMapper
}
