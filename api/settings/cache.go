package settings

import "time"

// Redis mapeia as configurações para uso do redis
type Redis struct {
	// Address            []string      `mapstructure:"address"`
	// DialTimeout        time.Duration `mapstructure:"idle_timeout_secs"`
	// IdleCheckFrequency time.Duration `mapstructure:"idle_check_freq_secs"`
	// IdleTimeout        time.Duration `mapstructure:"idle_timeout_secs"`
	// MaxIdle            int           `mapstructure:"max_idle"`
	// MaxRedirects       int           `mapstructure:"max_redirects"`
	// Passwd             string        `mapstructure:"password"`
	// PoolSize           int           `mapstructure:"pool_size"`
	// PoolTimeout        time.Duration `mapstructure:"pool_timeout"`
	// Port               string        `mapstructure:"port"`
	// ReadOnly           bool          `mapstructure:"read_only"`
	// ReadTimeout        time.Duration `mapstructure:"read_timeout_secs"`
	// RouteByLatency     bool          `mapstructure:"router_by_latency"`
	// WriteTimeout       time.Duration `mapstructure:"write_timeout_secs"`
	Address     []string      `mapstructure:"address"`
	IdleTimeout time.Duration `mapstructure:"idle_timeout_secs"`
	MasterName  string        `mapstructure:"master_name"`
	MaxIdle     int           `mapstructure:"max_idle"`
	Passwd      string        `mapstructure:"password"`
	PoolSize    int           `mapstructure:"pool_size"`
	PoolTimeout time.Duration `mapstructure:"pool_timeout"`
	Port        string        `mapstructure:"port"`
	Sentinel    bool          `mapstructure:"sentinel"`
}
