package settings

// Cors é usado para garantir allow_origin e outras regras vinda no request
type Cors struct {
	AllowCredentials bool     `mapstructure:"allow_credentials"`
	Allowed          []string `mapstructure:"allowed"`
	AllowedHeaders   []string `mapstructure:"allowed_headers"`
	AllowedMethods   []string `mapstructure:"allowed_methods"`
}
