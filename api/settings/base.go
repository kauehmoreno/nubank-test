package settings

// Settings load all settings defined on api
type Settings struct {
	DB    DB    `mapstructure:"db"`
	Cors  Cors  `mapstructure:"cors"`
	Redis Redis `mapstructure:"redis"`
	Queue Queue `mapstructure:"queue"`
	base  `mapstructure:",squash"`
}

type base struct {
	Ambiente string `mapstructure:"ambiente"`
	Host     string `mapstructure:"host"`
	Service  string `mapstructure:"service"`
	Port     string `mapstructure:"port"`
}
