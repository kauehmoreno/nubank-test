package settings

import "time"

// DatabaseSettings set all database access config
type DatabaseSettings struct {
	ConnMaxLifetime time.Duration `mapstructure:"conn_max_lifetime"`
	Host            string        `mapstructure:"host"`
	MaxConn         int           `mapstructure:"max_conn"`
	MaxIdle         int           `mapstructure:"max_idle"`
	Name            string        `mapstructure:"name"`
	Password        string        `mapstructure:"password"`
	Timeout         string        `mapstructure:"timeout"`
	User            string        `mapstructure:"user"`
	Port            string        `mapstructure:"port"`
}

// DB expose config from both read and write DB
type DB struct {
	Read  DatabaseSettings
	Write DatabaseSettings
}
