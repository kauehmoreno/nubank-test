package settings

import "time"

type Queue struct {
	Address     []string      `mapstructure:"address"`
	IdleTimeout time.Duration `mapstructure:"idle_timeout_secs"`
	MasterName  string        `mapstructure:"master_name"`
	MaxIdle     int           `mapstructure:"max_idle"`
	Passwd      string        `mapstructure:"password"`
	PoolSize    int           `mapstructure:"pool_size"`
	PoolTimeout time.Duration `mapstructure:"pool_timeout"`
	Port        string        `mapstructure:"port"`
	Sentinel    bool          `mapstructure:"sentinel"`
}
