// +test unit

package api_test

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/kauehmoreno/nubank-test/api"
)

var (
	url    string
	header = map[string]string{"Content-type": "application/json"}
	data   = struct {
		Message string `json:"message"`
		Status  int    `json:"status"`
	}{}
)

type ReqSuiteTestIntegrate struct {
	suite.Suite
}

func TestReqTestSuite(t *testing.T) {
	suite.Run(t, new(ReqSuiteTestIntegrate))
}

func (suite *ReqSuiteTestIntegrate) SetupTest() {
	url = "http://localhost:2122/teste"
}

func (suite *ReqSuiteTestIntegrate) TestPostError() {
	_, err := api.Post(url, header, nil)
	suite.Require().Error(err)
	suite.Require().Contains(err.Error(), "connect: connection refused", "Should refuse connection based on unavailable server")
}

func (suite *ReqSuiteTestIntegrate) TestGetStatus200() {
	ms := api.ServerMock(200, "Hello, client")
	res, err := api.Post(ms.URL, header, nil)
	suite.Require().NoError(err)
	defer res.Body.Close()

	bBody, erro := ioutil.ReadAll(res.Body)
	suite.Require().NoError(erro)

	json.Unmarshal(bBody, &data)
	suite.Require().Equal(data.Status, http.StatusOK)
}

func (suite *ReqSuiteTestIntegrate) TestRespectStatusCode() {
	ms := api.ServerMock(200, "12345")
	resp, err := api.Get(ms.URL, header)
	suite.Require().NoError(err)
	defer resp.Body.Close()
	bBody, err := ioutil.ReadAll(resp.Body)
	json.Unmarshal(bBody, &data)
	suite.Require().Equal(data.Status, http.StatusOK)
	suite.Require().Equal(data.Message, "12345")
}
