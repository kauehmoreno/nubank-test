package router

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/kauehmoreno/nubank-test/api/handler"
)

// New is used to define all api router public/auth ones
func New() http.Handler {
	router := httprouter.New()
	RecoveryCustomPanic(router)
	PublicRouter(router)
	HealtchCheckRouter(router)

	return router
}

// PublicRouter all public urls are exposed and defined here
func PublicRouter(r *httprouter.Router) {

	h := handler.NewCustomerHandler()
	r.POST("/customer/register", h.RegisterCustomer)
	r.GET("/customer/detail/:customerID", h.CustomerInfo)

	// transactions
	t := handler.NewTransactionHandler()
	r.POST("/transaction", t.InternalTransaction)
	r.POST("/transaction/in", t.ExternalTransactionIn)
	r.POST("/transaction/out", t.ExternalTransactionOut)
	r.GET("/transactions/:customerID", t.CustomerTransactions)

	// account
	acc := handler.NewAccountHandler()
	r.GET("/account/:customerID", acc.CostumerBalance)
}

func HealtchCheckRouter(r *httprouter.Router) {
	r.GET("/healthcheck", handler.HealthCheckHandler)
	r.GET("/healthcheck/cache", handler.CacheHealthCheckHandler)
	r.GET("/healthcheck/queue", handler.QueueHealthCheckHandler)
}

// RecoveryCustomPanic prevent from panic
func RecoveryCustomPanic(r *httprouter.Router) {
	r.PanicHandler = handler.PanicCustomRecovery
}
