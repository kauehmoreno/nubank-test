package workers

// Worker interface implementation of a simple worker
type Worker interface {
	On(queueName string) error
}
