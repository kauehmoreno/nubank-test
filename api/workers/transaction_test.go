package workers

import (
	"time"

	"gitlab.com/kauehmoreno/nubank-test/api/transaction"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"

	// . "github.com/onsi/gomega"
	"gitlab.com/kauehmoreno/nubank-test/api/account"
	"gitlab.com/kauehmoreno/nubank-test/api/customer"
	"gitlab.com/kauehmoreno/nubank-test/api/queue"
)

var _ = Describe("Worker package - transaction", func() {

	var (
		mockCtrl           *gomock.Controller
		mockQueue2         *queue.MockQueue
		mockAccountService *account.MockAcc
		mockCustomer       *customer.MockManager
		workertransaction  TransactionProcessed
		mockTransaction    *transaction.MockTransference
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockQueue2 = queue.NewMockQueue(mockCtrl)
		mockAccountService = account.NewMockAcc(mockCtrl)
		mockCustomer = customer.NewMockManager(mockCtrl)
		mockTransaction = transaction.NewMockTransference(mockCtrl)
		workertransaction = NewTransactionProcessed(
			TickerConfig(time.NewTicker(time.Second*1)),
			WithAccountManager(mockAccountService),
			WithCustomerManager(mockCustomer),
			WithTransactionManager(mockTransaction),
		)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Describe("Queue listenner tests", func() {
		Context("Transaction created worker", func() {
			It("should verify length of queue first", func() {
				mockQueue2.EXPECT().Len(queue.TrasanctionCreated).Return(int64(0)).AnyTimes()
				// Expect()
				go func() {
					defer GinkgoRecover()
					workertransaction.On(queue.TrasanctionCreated, mockQueue2)
				}()
				time.Sleep(time.Second * 3)
			})
		})
		Context("AddAmount", func() {
			It("Should try to outcome first", func() {
				// tr := transaction.New(db.GenerateUUID(), db.GenerateUUID(), transaction.WithAmount(102.12))
				mockQueue2.EXPECT().Len(queue.TrasanctionCreated).Return(int64(1)).AnyTimes()
				mockQueue2.EXPECT().GetMessage(queue.TrasanctionCreated, gomock.Any()).Return(nil).AnyTimes()
				mockAccountService.EXPECT().OutCome(gomock.Any(), gomock.Any()).Return(nil).Times(1)
				mockAccountService.EXPECT().InCome(gomock.Any(), gomock.Any()).Return(nil).Times(1)
				mockTransaction.EXPECT().ChangeStatus(gomock.Any(), transaction.Accept).Return(nil).Times(1)

				go func() {
					defer GinkgoRecover()
					workertransaction.On(queue.TrasanctionCreated, mockQueue2)
				}()
				time.Sleep(time.Second * 3)
			})
		})
	})

})
