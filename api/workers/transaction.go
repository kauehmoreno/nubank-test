package workers

import (
	"time"

	"github.com/cenkalti/backoff"

	"gitlab.com/kauehmoreno/nubank-test/api/customer"
	"gitlab.com/kauehmoreno/nubank-test/api/transaction"

	"github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kauehmoreno/nubank-test/api/account"
	"gitlab.com/kauehmoreno/nubank-test/api/queue"
)

// TransactionProcessed worker responsable to deduct and add balance from an account after a transaction created
// it will also change transaction status
type TransactionProcessed struct {
	t  *time.Ticker
	am account.Acc
	cm customer.Manager
	tm transaction.Transference
}

// Config set all config on TransactionProcessed worker
type Config func(t *TransactionProcessed)

// TickerConfig allow client to change time Ticker of worker execution
// it will make it easier to test and control time execution
func TickerConfig(t *time.Ticker) Config {
	return func(tr *TransactionProcessed) {
		tr.t = t
	}
}

// WithAccountManager allow account manager inside worker
func WithAccountManager(am account.Acc) Config {
	return func(tr *TransactionProcessed) {
		tr.am = am
	}
}

// WithCustomerManager allow customer manager inside worker
func WithCustomerManager(cm customer.Manager) Config {
	return func(tr *TransactionProcessed) {
		tr.cm = cm
	}
}

// WithTransactionManager allow transaction manager inside worker
func WithTransactionManager(tm transaction.Transference) Config {
	return func(tr *TransactionProcessed) {
		tr.tm = tm
	}
}

// NewTransactionProcessed start worker
func NewTransactionProcessed(conf ...Config) TransactionProcessed {
	tr := TransactionProcessed{
		t: time.NewTicker(time.Second * 30),
	}

	for _, config := range conf {
		config(&tr)
	}
	return tr
}

// On intialize worker to listen and react on every element into queue
func (trans TransactionProcessed) On(queueName string, q queue.Queue) error {
	log.Infof("[WORKER][TransactionProcessed] worker is active on queue %s", queueName)
	defer trans.t.Stop()
	for t := range trans.t.C {
		log.Infof("[WORKER][TransactionProcessed] Starting createAccount worker at %v", t)
		trans.startListenner(queueName, q)
		log.Infof("[WORKER][TransactionProcessed] Stoping createAccount worker at %v", t)
	}
	return nil
}

// startListenner that will add and deduct money from customer account balance
func (trans TransactionProcessed) startListenner(queueName string, q queue.Queue) {
	var tr transaction.Transaction
	length := q.Len(queueName)
	if length > 0 {
		for n := 0; n < int(length); n++ {
			if err := q.GetMessage(queueName, &tr); err != nil {
				log.Errorf("[TransactionProcessed] error on get message from queue %s error %v", queueName, err)

				fn := func() error {
					if err := q.GetMessage(queueName, &tr); err != nil && err != redis.Nil {
						return err
					}
					return nil
				}

				if err := backoff.Retry(fn, backoff.NewExponentialBackOff()); err != nil {
					log.Errorf("[WORKER][TransactionProcessed] Fail on retry finishing %v", err)
					break
				}
				continue
			}
			trans.AddOrDeductAmount(tr)
		}
	}
	time.Sleep(time.Second * 10)
}

// AddOrDeductAmount based on some rules will call add ou deduct on certain customer
// it checks if is a external transaction or not
func (trans TransactionProcessed) AddOrDeductAmount(tr transaction.Transaction) error {
	if tr.IsExternalTransaction() {
		if tr.SenderAccID != "" {
			if err := trans.deductAmount(tr.SenderAccID, tr.Amount); err != nil {
				if err := trans.tm.ChangeStatus(tr.ID, transaction.Deny); err != nil {
					log.Errorf("[TransactionProcessed] error on change status of a transaction %s - error %v", tr.ID, err)
				}
				return err
			}
			return trans.tm.ChangeStatus(tr.ID, transaction.Accept)

		}
		return trans.addAmount(tr.ID, tr.ReceiverAccID, tr.Amount)
	}

	if err := trans.deductAmount(tr.SenderAccID, tr.Amount); err != nil {
		defer trans.tm.ChangeStatus(tr.ID, transaction.Deny)
		return err
	}

	return trans.addAmount(tr.ID, tr.ReceiverAccID, tr.Amount)
}

// addAmount allows receivers to get money update or their balance
func (trans TransactionProcessed) addAmount(transactionID string, accountID string, amount float64) error {
	if err := trans.am.InCome(accountID, amount); err != nil {
		log.WithFields(log.Fields{"error": err, "accountid": accountID, "amount": amount}).Error("Error on add amount to acountID")

		fn := func() error {
			return trans.am.InCome(accountID, amount)
		}

		if err := backoff.Retry(fn, backoff.NewExponentialBackOff()); err != nil {
			return trans.tm.ChangeStatus(transactionID, transaction.Canceled)
		}

		return trans.tm.ChangeStatus(transactionID, transaction.Accept)

	}
	return trans.tm.ChangeStatus(transactionID, transaction.Accept)
}

// deductAmount senders will have their balance deduct the exacly amount of money transfered
func (trans TransactionProcessed) deductAmount(accountID string, amount float64) error {
	return trans.am.OutCome(accountID, amount)
}
