package workers

import (
	"time"

	"gitlab.com/kauehmoreno/nubank-test/api/db"

	"github.com/cenkalti/backoff"
	"github.com/go-redis/redis"

	log "github.com/sirupsen/logrus"

	"gitlab.com/kauehmoreno/nubank-test/api/account"
	"gitlab.com/kauehmoreno/nubank-test/api/customer"
	"gitlab.com/kauehmoreno/nubank-test/api/queue"
)

// CreateAccount implements worker and listen all events on queueName
type CreateAccount struct {
	t *time.Ticker
}

// Options is used to allow client to put extra options on createAccount worker
// configuration
type Options func(ca *CreateAccount)

// WithTicker allow client to change time Ticker of worker execution
// it will make it easier to test and control time execution
func WithTicker(t *time.Ticker) Options {
	return func(ca *CreateAccount) {
		ca.t = t
	}
}

// NewCreateAccount apply options on worker and then return it back to client
func NewCreateAccount(options ...Options) CreateAccount {
	ca := CreateAccount{
		t: time.NewTicker(time.Second * 30),
	}
	for _, opt := range options {
		opt(&ca)
	}
	return ca
}

// On intialize worker to listen and react on every element into queue
// TODO REFACTOR
func (ca CreateAccount) On(queueName string, q queue.Queue, service account.Acc) error {
	log.Infof("[WORKER] createAccount worker is active on queue %s", queueName)
	defer ca.t.Stop()
	for t := range ca.t.C {
		log.Infof("[WORKER] Starting createAccount worker at %v", t)
		ca.startListener(queueName, q, service)
		log.Infof("[WORKER] Stoping createAccount worker at %v", t)
	}
	return nil
}

func (ca CreateAccount) startListener(queueName string, q queue.Queue, service account.Acc) {
	var c customer.Customer
	qtd := q.Len(queueName)
	if qtd > 0 {
		for n := 0; n < int(qtd); n++ {
			if err := q.GetMessage(queueName, &c); err != nil && err != redis.Nil {
				log.Errorf("[WORKER] Erro on get message into queue %s - error: %v \n", queueName, err)
				fn := func() error {
					if err := q.GetMessage(queueName, &c); err != nil && err != redis.Nil {
						log.Errorf("[WORKER] backoff policy - error on GetMessage from queue again retrying ... %v", err)
						return err
					}
					return nil
				}

				failureCallback := func() error {
					return q.Push(queueName, c)
				}
				sucessCallback := func() error { return nil }
				if err := retry(fn, sucessCallback, failureCallback); err != nil {
					log.Errorf("[WORKER] Fail on retry and callback %v", err)
					break
				}
				continue
			}
			acc := account.New(c.AccountID)
			if err := service.Create(acc); err != nil && err != db.ErrDuplicated {
				log.Errorf("[WORKER] Erro on create account %s based on customer %s - error: %v", c.AccountID, c.ID, err)
				fn := func() error {
					if err := service.Create(acc); err != nil {
						log.Errorf("[WORKER] backoff policy - error on save account again retrying ... %v", err)
						return err
					}
					return nil
				}
				failureCallback := func() error {
					return q.Push(queueName, acc)
				}
				sucessCallback := func() error { return nil }
				if err := retry(fn, sucessCallback, failureCallback); err != nil {
					log.Errorf("[WORKER] Fail on retry and callback %v", err)
					continue
				}
			}
			continue
		}
	}
	time.Sleep(time.Second * 10)
}

func retry(fn func() error, sucessCallback func() error, failureCallback func() error) error {
	log.Info("[WORKER] starting backoff policy...")
	if err := backoff.Retry(fn, backoff.NewExponentialBackOff()); err != nil {
		log.Errorf("[WORKER] error on trying to get element from queue ... %v", err)
		return failureCallback()
	}
	return sucessCallback()
}
