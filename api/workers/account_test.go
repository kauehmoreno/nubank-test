package workers_test

import (
	"errors"
	"time"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/kauehmoreno/nubank-test/api/account"
	"gitlab.com/kauehmoreno/nubank-test/api/queue"
	"gitlab.com/kauehmoreno/nubank-test/api/workers"
)

var _ = Describe("Worker package - account create", func() {

	var (
		mockCtrl           *gomock.Controller
		mockQueue          *queue.MockQueue
		mockAccountService *account.MockAcc
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockQueue = queue.NewMockQueue(mockCtrl)
		mockAccountService = account.NewMockAcc(mockCtrl)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Describe("Queue behavior test", func() {
		Context("Create Account worker", func() {
			It("Should execute function after specfied amount of time and keep executing", func() {
				mockQueue.EXPECT().GetMessage("teste", gomock.Any()).Return(nil).AnyTimes().Do(func(qName string, data interface{}) {
					Expect(qName).Should(Equal("teste"))
				})
				// mockQueue.EXPECT().Len("teste").Return(int64(10)).Times(1)

				t := time.NewTicker(time.Second * 1)
				w := workers.NewCreateAccount(workers.WithTicker(t))

				go func() {
					defer GinkgoRecover()
					w.On("teste", mockQueue, account.NewManager())
				}()
				time.Sleep(time.Second * 3)
			})
			It("Should execute save if Len return more than one item and getMessage returns no error", func() {
				mockQueue.EXPECT().GetMessage("teste", gomock.Any()).Return(nil).AnyTimes().Do(func(qName string, data interface{}) {
					Expect(qName).Should(Equal("teste"))
				})
				mockQueue.EXPECT().Len("teste").Return(int64(10)).AnyTimes()
				mockAccountService.EXPECT().Create(gomock.Any()).Return(nil).AnyTimes()

				t := time.NewTicker(time.Second * 1)
				w := workers.NewCreateAccount(workers.WithTicker(t))

				go func() {
					defer GinkgoRecover()
					w.On("teste", mockQueue, mockAccountService)
				}()
				time.Sleep(time.Second * 3)
			})
			It("Shoudnt execute getMessage if len returns less than one item on queue", func() {
				mockQueue.EXPECT().GetMessage("teste", gomock.Any()).Times(0)
				mockQueue.EXPECT().Len("teste").Return(int64(0)).AnyTimes()
				mockAccountService.EXPECT().Create(gomock.Any()).Times(0)

				t := time.NewTicker(time.Second * 1)
				w := workers.NewCreateAccount(workers.WithTicker(t))
				go func() {
					defer GinkgoRecover()
					w.On("teste", mockQueue, mockAccountService)
				}()
				time.Sleep(time.Second * 3)
			})
			It("Should call retry whenever a msg return error for the first time", func() {
				mockQueue.EXPECT().GetMessage("teste", gomock.Any()).Times(1).DoAndReturn(func(name string, v interface{}) {
					Expect(name).Should(Equal("teste"))
				}).Return(errors.New("error")).AnyTimes()
				mockQueue.EXPECT().Len("teste").Return(int64(1)).AnyTimes()
				mockAccountService.EXPECT().Create(gomock.Any()).Times(0)

				t := time.NewTicker(time.Second * 1)
				w := workers.NewCreateAccount(workers.WithTicker(t))
				go func() {
					defer GinkgoRecover()
					w.On("teste", mockQueue, mockAccountService)
				}()
				time.Sleep(time.Second * 3)
			})

		})
	})
})
