package account

import (
	"errors"

	"github.com/go-redis/redis"

	gomock "github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/kauehmoreno/nubank-test/api/cache"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

var _ = Describe("Repository test on package account ", func() {

	var (
		mockCache *cache.MockCache
		mockDB    *db.MockDB
		mockCtrl  *gomock.Controller
		repo      repository
		id        = db.GenerateUUID()
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockCache = cache.NewMockCache(mockCtrl)
		mockDB = db.NewMockDB(mockCtrl)
		repo.CacheRep = mockCache
		repo.InnerRep = dbRepo{
			db: mockDB,
		}
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Describe("db_context file", func() {
		Context("Testing account method of Repository", func() {
			It("Should return from cache whenever there is data on it and does not go to innerRep", func() {
				var acc Account
				mockCache.EXPECT().Get(gomock.Any(), &acc).Return(nil).Times(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Times(0)
				_, err := repo.Account(id)
				Expect(err).To(BeNil())
			})
			It("Should execute db after cache return error besides matchs parameter inside db function and then execute set on cache", func() {
				var acc Account
				mockCache.EXPECT().Get(gomock.Any(), &acc).Return(errors.New("error")).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Times(1).Do(func(acc *Account, query, nID string) {
					Expect(nID).Should(Equal(id))
					Expect(query).Should(Equal("/*getAccount.Account */ SELECT id, number, agency, balance FROM account WHERE id=?"))
				}).Return(nil)
				_, err := repo.Account(id)
				Expect(err).To(BeNil())
			})
			It("Should execute db and set on cache with cache return nil when keys does not exists", func() {

				var acc Account
				mockCache.EXPECT().Get(gomock.Any(), &acc).Return(redis.Nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Times(1).Do(func(acc *Account, query, nID string) {
					Expect(nID).Should(Equal(id))
					Expect(query).Should(Equal("/*getAccount.Account */ SELECT id, number, agency, balance FROM account WHERE id=?"))
				}).Return(nil)
				_, err := repo.Account(id)
				Expect(err).To(BeNil())
			})

			It("Should return error if db does not execut properly and then not execute set on cache", func() {

				var acc Account
				mockCache.EXPECT().Get(gomock.Any(), &acc).Return(redis.Nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).Times(0)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Times(1).Return(errors.New("error"))
				_, err := repo.Account(id)
				Expect(err).ToNot(BeNil())
			})
		})
		Context("Testing Create method of repository", func() {
			It("Should first try to creat on db and then set value into cache", func() {
				acc := New(id)
				mockResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(mockResult, nil)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(1)
				err := repo.Create(acc)
				Expect(err).To(BeNil())
			})
			It("Should match values into db when call create", func() {
				acc := New(id)
				mockResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Do(func(query, id string, number uint32, agency uint16, balance float64) {
					Expect(query).Should(Equal("/*save.account */ INSERT INTO account (id, number, agency, balance) VALUES ( ?, ?, ?, ?)"))
					Expect(id).Should(Equal(acc.ID))
					Expect(number).Should(Equal(acc.GetNumber()))
					Expect(agency).Should(Equal(acc.GetAgency()))
					Expect(balance).Should(Equal(acc.GetBalance()))
				}).Return(mockResult, nil)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(1)
				err := repo.Create(acc)
				Expect(err).To(BeNil())
			})
			It("Should not execute cache if db returns error", func() {
				acc := New(id)
				mockResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(mockResult, errors.New("error"))
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(0)
				err := repo.Create(acc)
				Expect(err).ToNot(BeNil())
			})
			It("Should return error if no rows got affected by db operation - drive does not return error", func() {
				acc := New(id)
				mockResult := db.NewMockSQLResult(db.ErrNoAffectedRow, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(mockResult, nil)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(0)
				err := repo.Create(acc)
				Expect(err).ToNot(BeNil())
				Expect(err).Should(Equal(db.ErrNoAffectedRow))
			})
		})
		Context("Income balance method", func() {
			It("Should get account from repository and then addAmount to then update value on db and then on cache", func() {
				mockResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(mockResult, nil)
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Do(func(key string, acc *Account) {
					nAcc := New(db.GenerateUUID())
					acc = &nAcc
				}).Return(nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(1)
				err := repo.InCome(id, 232.32)
				Expect(err).To(BeNil())
			})

			It("Should match params on db operation", func() {
				mockResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Do(func(query string, balance float64, accID string) {
					Expect(query).Should(Equal("/*update.account */ UPDATE account SET balance=? WHERE id=?"))
					Expect(accID).Should(Equal(id))
					Expect(balance).Should(Equal(232.32))
				}).Return(mockResult, nil)
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(1)
				err := repo.InCome(id, 232.32)
				Expect(err).To(BeNil())
			})
			It("Should return err with no rows get affected by operation on db - driver does not return error", func() {
				mockResult := db.NewMockSQLResult(db.ErrNoAffectedRow, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(mockResult, nil)
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(0)
				err := repo.InCome(id, 232.32)
				Expect(err).ToNot(BeNil())
			})
			It("Should return error with db returns an error from drive", func() {
				mockResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(mockResult, errors.New("error on drive"))
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(0)
				err := repo.InCome(id, 232.32)
				Expect(err).ToNot(BeNil())
				Expect(err.Error()).Should(Equal("error on drive"))
			})

			It("Should return error if get account returns error and not execute db and cache", func() {
				mockResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(mockResult, nil).Times(0)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(0)

				// mock execution on account
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(errors.New("error cache")).Times(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("error db")).Times(1)
				err := repo.InCome(id, 232.32)
				Expect(err).ToNot(BeNil())
				Expect(err.Error()).Should(Equal("error db"))
			})
		})

		Context("OutCome balance method", func() {
			It("Should get from account and then deduct from it", func() {
				nAcc := New(db.GenerateUUID())
				nAcc.Balance = 400.21

				mockResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(mockResult, nil).Times(0)
				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).DoAndReturn(func(key string, acc *Account) {
					acc = &nAcc
				}).Return(nil).Times(1)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(0)
				err := repo.OutCome(id, 232.32)
				Expect(err).ToNot(BeNil())
				Expect(err).Should(Equal(ErrEmptyBalance))
			})

			It("Should return err if get from account return error", func() {
				mockResult := db.NewMockSQLResult(nil, 0, 1)
				mockDB.EXPECT().Exec(gomock.Any(), gomock.Any()).Return(mockResult, nil).Times(0)
				mockCache.EXPECT().Set(gomock.Any(), gomock.Any(), cache.NoExpiration).Return(nil).Times(0)

				mockCache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(errors.New("error cache")).Times(1)
				mockDB.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("error db")).Times(1)
				err := repo.OutCome(id, 232.32)
				Expect(err).ToNot(BeNil())
				Expect(err.Error()).Should(Equal("error db"))
			})
		})
	})
})
