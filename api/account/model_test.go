package account_test

import (
	"math/rand"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/kauehmoreno/nubank-test/api/account"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

var _ = Describe("package account - model", func() {
	Describe("Account Model", func() {
		Context("New", func() {
			It("Should given an id return an struct with default values", func() {
				id := db.GenerateUUID()
				acc := account.New(id)
				Expect(acc.GetAgency()).Should(Equal(uint16(1010)))
				Expect(acc.GetBalance()).Should(BeZero())
			})
			It("Shoud have a account number", func() {
				id := db.GenerateUUID()
				acc := account.New(id)
				Expect(acc.GetNumber()).Should(BeNumerically(">", uint16(0)))
			})
		})
		Context("Create account number", func() {
			It("Should generate different numbers on every call", func() {
				iter := rand.Perm(1000)
				validator := make(map[uint32]bool)
				for range iter {
					n := account.CreateNumber()
					if ok := validator[n]; ok {
						Fail("Should generate repeated numbers")
						break
					}
					validator[n] = true
				}
			})
		})
		Context("AddAmount", func() {
			It("Should add amount on balance", func() {
				acc := account.New(db.GenerateUUID())
				acc.AddAmount(321.22)
				acc.AddAmount(22.88)
				Expect(acc.GetBalance()).Should(Equal(float64(344.10)))
			})
			It("Should properly add small values", func() {
				acc := account.New(db.GenerateUUID())
				acc.AddAmount(321.22)
				acc.AddAmount(0.88)
				Expect(acc.GetBalance()).Should(Equal(float64(322.1)))
			})
		})
		Context("DeductAmount", func() {
			It("Should return ErrEmptyBalance if balance is zero", func() {
				acc := account.New(db.GenerateUUID())
				err := acc.DeductAmount(12.20)
				Expect(err).To(Equal(account.ErrEmptyBalance))
			})

			It("Should return ErrNoEnoughBalance if balance is lower then amount to deduct", func() {
				acc := account.New(db.GenerateUUID())
				acc.AddAmount(10.14)
				err := acc.DeductAmount(12.20)
				Expect(err).To(Equal(account.ErrNoEnoughBalance))
			})

			It("Should deduct just fine when amount is equal balance", func() {
				acc := account.New(db.GenerateUUID())
				acc.AddAmount(10.14)
				err := acc.DeductAmount(10.14)
				Expect(err).To(BeNil())
				Expect(acc.GetBalance()).Should(Equal(float64(0)))
			})
			It("Should deduct just fine when amount is greater than balance", func() {
				acc := account.New(db.GenerateUUID())
				acc.AddAmount(10.14)
				err := acc.DeductAmount(9.14)
				Expect(err).To(BeNil())
				Expect(acc.GetBalance()).Should(Equal(float64(1)))
			})
		})
	})
})
