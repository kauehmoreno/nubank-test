package account

import (
	"encoding/binary"
	"errors"
	"math/rand"
)

var (
	// ErrNoEnoughBalance is an error which shows that user cannot operate subAmount when balance is lower than amount
	ErrNoEnoughBalance = errors.New("Account does not have enough balance to borrow money")
	// ErrEmptyBalance shows whenever an account has zero o lower value
	ErrEmptyBalance = errors.New("Account does not have balance - equals zero")
	// EmptyBalance represent zero value on balance
	EmptyBalance = float64(0)
)

// AgencyNum ...
const AgencyNum uint16 = 1010

// ExternalAccountInfo extra info if transaction is between other banks
// its necessary to provide extra information
type ExternalAccountInfo struct {
	Bank       string `json:"bank_name" db:"bank_name"`
	BankNumber uint32 `json:"bank_number" db:"bank_number"`
	BankAgency uint16 `json:"bank_agency" db:"bank_agency"`
	UserName   string `json:"user_name" db:"user_name"`
	Identifier string `json:"user_identifier" db:"user_identifier"`
}

// Account - model representation of user account
type Account struct {
	ID      string  `json:"id" db:"id"`
	Number  uint32  `json:"number" db:"number"`
	Agency  uint16  `json:"agency" db:"agency"`
	Balance float64 `json:"balance" db:"balance"`
}

func New(id string) Account {
	return Account{
		ID:      id,
		Number:  CreateNumber(),
		Agency:  AgencyNum,
		Balance: 0,
	}
}

func (a *Account) GetBalance() float64 {
	return a.Balance
}

func (a *Account) AddAmount(amount float64) error {
	a.Balance += amount
	return nil
}

func (a *Account) DeductAmount(amount float64) error {
	if a.Balance <= EmptyBalance {
		return ErrEmptyBalance
	}
	if a.Balance < amount {
		return ErrNoEnoughBalance
	}

	a.Balance -= amount
	return nil
}

func (a *Account) GetAgency() uint16 {
	return a.Agency
}

func (a *Account) GetNumber() uint32 {
	return a.Number
}

func CreateNumber() uint32 {
	buf := make([]byte, 8)
	rand.Read(buf)
	return binary.LittleEndian.Uint32(buf)
}
