package account

import (
	"gitlab.com/kauehmoreno/nubank-test/api/cache"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
)

type Acc interface {
	Account(id string) (Account, error)
	Create(a Account) error
	InCome(accID string, amount float64) error
	OutCome(accID string, amount float64) error
}

func NewManager() Acc {
	return repository{
		CacheRep: cache.GetInstance(),
		InnerRep: dbRepo{
			db: db.GetInstance(),
		},
	}
}

type dbRepo struct {
	db db.DB
}

func (d dbRepo) getAccount(id string) (Account, error) {
	query := "/*getAccount.Account */ SELECT id, number, agency, balance FROM account WHERE id=?"
	var acc Account
	if err := d.db.Get(&acc, query, id); err != nil {
		return acc, err
	}
	return acc, nil
}

func (d dbRepo) save(a Account) error {
	query := "/*save.account */ INSERT INTO account (id, number, agency, balance) VALUES ( ?, ?, ?, ?)"
	result, err := d.db.Exec(query, a.ID, a.Number, a.Agency, a.Balance)
	if err != nil {
		return err
	}

	if affected, _ := result.RowsAffected(); affected == db.NoRowsAffected {
		return db.ErrNoAffectedRow
	}
	return nil
}

// Save a Customer creation on db and build cache strategy
func (d dbRepo) update(accID string, balance float64) error {
	query := "/*update.account */ UPDATE account SET balance=? WHERE id=?"
	result, err := d.db.Exec(query, balance, accID)
	if err != nil {
		return err
	}
	if affected, _ := result.RowsAffected(); affected == db.NoRowsAffected {
		return db.ErrNoAffectedRow
	}

	return nil
}

type repository struct {
	CacheRep cache.Cache
	InnerRep dbRepo
}

func (r repository) Account(id string) (Account, error) {
	var acc Account

	k := cache.FormatKey(cache.KeyCustomerAccount, id)
	if err := r.CacheRep.Get(k, &acc); err != nil {
		customerAccount, erro := r.InnerRep.getAccount(id)
		if erro != nil {
			return customerAccount, erro
		}
		defer r.CacheRep.Set(k, customerAccount, cache.NoExpiration)
		return customerAccount, nil
	}
	return acc, nil
}

func (r repository) Create(a Account) error {
	if err := r.InnerRep.save(a); err != nil {
		return err
	}

	k := cache.FormatKey(cache.KeyCustomerAccount, a.ID)
	defer r.CacheRep.Set(k, a, cache.NoExpiration)

	return nil

}

// InCome change customer balance whenever it receive a transaction from another customer
// it will set value into db and then set on cache
// if fails on cache goes on and cache will be set on next get operation
func (r repository) InCome(accID string, amount float64) error {
	acc, err := r.Account(accID)
	if err != nil {
		return err
	}

	acc.AddAmount(amount)
	if err := r.InnerRep.update(accID, acc.GetBalance()); err != nil {
		return err
	}
	k := cache.FormatKey(cache.KeyCustomerAccount, accID)
	defer r.CacheRep.Set(k, acc, cache.NoExpiration)
	return nil
}

// OutCome change customer balance whenever it send money to a customer
func (r repository) OutCome(accID string, amount float64) error {
	acc, err := r.Account(accID)
	if err != nil {
		return err
	}

	if err := acc.DeductAmount(amount); err != nil {
		return err
	}

	if err := r.InnerRep.update(accID, acc.GetBalance()); err != nil {
		return err
	}
	k := cache.FormatKey(cache.KeyCustomerAccount, accID)
	defer r.CacheRep.Set(k, acc, cache.NoExpiration)
	return nil
}
