package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/kauehmoreno/nubank-test/api/account"
	"gitlab.com/kauehmoreno/nubank-test/api/customer"
	"gitlab.com/kauehmoreno/nubank-test/api/db"
	"gitlab.com/kauehmoreno/nubank-test/api/queue"
	"gitlab.com/kauehmoreno/nubank-test/api/router"
	"gitlab.com/kauehmoreno/nubank-test/api/settings"
	"gitlab.com/kauehmoreno/nubank-test/api/transaction"
	"gitlab.com/kauehmoreno/nubank-test/api/workers"

	"github.com/phyber/negroni-gzip/gzip"

	"github.com/rs/cors"
	"github.com/urfave/negroni"
)

// Cors define api cors
func Cors() *cors.Cors {
	return cors.New(cors.Options{
		AllowedOrigins: settings.Get().Cors.Allowed,
		AllowedMethods: settings.Get().Cors.AllowedMethods,
		AllowedHeaders: settings.Get().Cors.AllowedHeaders,
	})
}

func main() {

	n := negroni.New(negroni.NewRecovery(), Cors())
	n.Use(gzip.Gzip(gzip.BestSpeed))

	startWorkers()

	log.Info("RUNING API")

	rt := router.New()
	n.UseHandler(rt)
	db.CreateTables()
	host := fmt.Sprintf("%s:%s", settings.Get().Service, settings.Get().Port)
	n.Run(host)
}

func startWorkers() {
	w := workers.NewCreateAccount()
	go w.On(queue.CreateAccount, queue.GetInstance(), account.NewManager())
	w2 := workers.NewTransactionProcessed(
		workers.WithAccountManager(account.NewManager()),
		workers.WithCustomerManager(customer.NewManager()),
		workers.WithTransactionManager(transaction.NewManager()),
	)
	go w2.On(queue.TrasanctionCreated, queue.GetInstance())
}
